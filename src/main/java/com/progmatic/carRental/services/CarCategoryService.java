/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services;

import com.progmatic.carRental.entities.CarCategory;
import com.progmatic.carRental.repositories.CarCategoryRepository;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author User
 */
@Service
public class CarCategoryService {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    CarCategoryRepository carCategoryRepository;

    public List<CarCategory> findAll() {
        return carCategoryRepository.findAll();
    }

    @Transactional
    public void addCarCategory(CarCategory carCategory) {
        entityManager.persist(carCategory);
    }

    public List<CarCategory> findCarCategory() {
           List resultList = entityManager.createQuery("select m from CarCategory m")
                .getResultList();
        return resultList;
    }

    public void save(CarCategory carCategory) {
        carCategoryRepository.save(carCategory);
    }
    
    public byte[] getCarCategoryPhoto(int imageId) {
        CarCategory carCategory = carCategoryRepository.findById(imageId);
        if (carCategory != null) {
        return carCategory.getPhoto();
        }
        
        return new byte[]{};
    }
    
    public CarCategory findByCategoryId(int carCategory){
        return carCategoryRepository.findById(carCategory);    
    }

}
