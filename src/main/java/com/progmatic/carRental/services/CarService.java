/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services;

import com.progmatic.carRental.entities.Car;
import com.progmatic.carRental.entities.CarCategory;
import com.progmatic.carRental.enums.EngineTypeEnum;
import com.progmatic.carRental.repositories.CarRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author User
 */
@Service
public class CarService {

    @Autowired
    CarRepository carRepository;

    @PersistenceContext
    EntityManager entityManager;

    public Car FindACarByID(int id) {
        Query query = entityManager.createQuery("SELECT c FROM Car c WHERE c.id = :id")
                .setParameter("id", id);
        List<Car> resultList = query.getResultList();
        return resultList.get(0);
    }

    public List<CarCategory> findCarCategory(Integer msgMax) {
        msgMax = Math.abs(msgMax);
        List resultList = entityManager.createQuery("select c from CarCategory c")
                .getResultList();
        return resultList;
    }

    public List<EngineTypeEnum> getEngineTypeList() {
        List<EngineTypeEnum> engineTypeList = Arrays.asList(EngineTypeEnum.values());
        //System.out.println("A lista elemei: " + engineTypeList);
        return engineTypeList;
    }

    public List<Car> findAllCarList() {
        List resultList = entityManager.createQuery("select c from Car c")
                .getResultList();
        return resultList;
    }

    public List<Integer> findAllCarPrice() {
        List resultList = 
                entityManager.createQuery("select cc.price from CarCategory cc inner join Car c on cc.id = c.carCategoryId")
                .getResultList();
        return resultList;
    }
       public List<String> findAllCategoryNameList() {
        List resultList = 
                entityManager.createQuery("select cc.name from CarCategory cc inner join Car c on cc.id = c.carCategoryId")
                .getResultList();
        return resultList;
    }

    public List<Car> findNotDeletedCarList() {
        List resultList = entityManager.createQuery("select c from Car c WHERE is_deleted=0")
                .getResultList();
        return resultList;
    }

    public void save(Car car) {
        carRepository.save(car);
    }

    public List<Car> getRandomCars(int number) {
        List<Car> allCars = carRepository.findAll();
        number = Math.min(number, allCars.size());
        HashSet<Integer> randomCarIds = new HashSet<>();
        while (randomCarIds.size() < number) {
            randomCarIds.add((int) (Math.random() * allCars.size()));
        }
        List<Car> randomCars = new ArrayList<>();
        for (Integer randomCarId : randomCarIds) {
            randomCars.add(allCars.get(randomCarId));

        }
        return randomCars;
    }

}
