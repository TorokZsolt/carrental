/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services;

import com.progmatic.carRental.dto.CarCategoryDto;
import com.progmatic.carRental.entities.CarCategory;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;

/**
 *
 * @author Molnár Richárd
 */
@Service
public class StatisticsService {

    @PersistenceContext
    EntityManager entityManager;

    public List<Integer> getYears(String chartId) {
        String status = "";
        String queryString = "";
        List resultlistByYear = new ArrayList<>();
        if (chartId.equals("finished_car")) {
            status = "FINISHED";
            queryString = "SELECT YEAR(r.rentStart),YEAR(r.rentEnd) FROM Rent r WHERE r.status =:s";
            resultlistByYear = entityManager.createQuery(queryString)
                .setParameter("s", status)
                .getResultList();
        } else if (chartId.equals("booked_car")) {
            status = "BOOKED";
            queryString = "SELECT YEAR(b.bookStart),YEAR(b.bookEnd) FROM Booking b";
            resultlistByYear = entityManager.createQuery(queryString)
                .getResultList();
        }else if (chartId.equals("rent_car")){
            status = "RENT";
            queryString = "SELECT YEAR(r.rentStart),YEAR(r.rentEnd) FROM Rent r WHERE r.status =:s";
            resultlistByYear = entityManager.createQuery(queryString)
                .setParameter("s", status)
                .getResultList();
        }
        Set<Integer> years = new HashSet<>();
        for (Object object : resultlistByYear) {
            Object[] objArray = (Object[]) object;
            for (int i = 0; i < objArray.length; i++) {
                Integer y = (Integer) objArray[i];
                years.add(y);
            }
        }
        List<Integer> sortedYears = new ArrayList<>();
        sortedYears.addAll(years);
        Collections.sort(sortedYears);
        return sortedYears;
    }

    public List<Integer> getMonths(String chartID, int year) {
        String status = "";
        String queryString = "";
        List resultlistByMonth = new ArrayList<>();
        if (chartID.equals("finished_car")) {
            status = "FINISHED";
            queryString = "SELECT MONTH(r.rentEnd), MONTH(r.rentEnd) FROM Rent r WHERE r.status =:s AND YEAR(r.rentEnd)=:y";
            resultlistByMonth = entityManager.createQuery(queryString)
                .setParameter("s", status)
                .setParameter("y", year)
                .getResultList();
        } else if (chartID.equals("booked_car")) {
            status = "BOOKED";
            queryString = "SELECT MONTH(b.bookStart),MONTH(b.bookEnd) FROM Booking b WHERE YEAR(b.bookEnd)=:y";
            resultlistByMonth = entityManager.createQuery(queryString)
                .setParameter("y", year)
                .getResultList();
        }else if (chartID.equals("rent_car")){
            status = "RENT";
            queryString = "SELECT MONTH(r.rentStart),MONTH(r.rentEnd) FROM Rent r WHERE r.status =:s AND YEAR(r.rentEnd)=:y";
            resultlistByMonth = entityManager.createQuery(queryString)
                .setParameter("s", status)
                .setParameter("y", year)
                .getResultList();
        }
        Set<Integer> months = new HashSet<>();
        for (Object object : resultlistByMonth) {
            Object[] objArray = (Object[]) object;
            for (int i = 0; i < objArray.length; i++) {
                Integer m = (Integer) objArray[i];
                months.add(m);
            }
        }
        List<Integer> sortedMonths = new ArrayList<>();
        sortedMonths.addAll(months);
        Collections.sort(sortedMonths);
        return sortedMonths;
    }
    
    
            



    public List<CarCategoryDto> getFinishedCarCategoriesByDate(int year, int month) {

        List resultList = entityManager.createQuery(
                "SELECT cc, count(cc) FROM CarCategory cc JOIN Rent r on cc.id = r.carCategoryId where r.status = 'FINISHED'"
                + "                       AND YEAR(r.rentEnd) = :y and MONTH(r.rentEnd) = :m GROUP BY r.carCategoryId")
                .setParameter("y", year)
                .setParameter("m", month)
                .getResultList();
        List<CarCategoryDto> finishedDtoList = new ArrayList<>();
        for (Object object : resultList) {
            Object[] objArray = (Object[]) object;
            CarCategory cc = (CarCategory) objArray[0];
            CarCategoryDto c = new CarCategoryDto(cc.getId(), cc.getName(), cc.getPrice(), cc.getDescription(), cc.getPhoto());
            Long count = (Long) objArray[1];
            c.setRentCountPerMonth(count);
            finishedDtoList.add(c);
        }

        return finishedDtoList;
    }

    public List<CarCategoryDto> getBookedCarCategoriesByDate(int year, int month) {

        List resultList = entityManager.createQuery(
                "SELECT cc, count(cc) FROM CarCategory cc JOIN Booking r on cc.id = (SELECT carCategoryId FROM Car c WHERE c.id = r.carId) where"
                + "                        ( YEAR(r.bookStart) = :y and MONTH(r.bookStart) = :m"
                + "                       OR YEAR(r.bookEnd) = :y and MONTH(r.bookEnd) = :m ) GROUP BY cc.id")
                .setParameter("y", year)
                .setParameter("m", month)
                .getResultList();
        List<CarCategoryDto> bookedDtoList = new ArrayList<>();
        for (Object object : resultList) {
            Object[] objArray = (Object[]) object;
            CarCategory cc = (CarCategory) objArray[0];
            CarCategoryDto c = new CarCategoryDto(cc.getId(), cc.getName(), cc.getPrice(), cc.getDescription(), cc.getPhoto());
            Long count = (Long) objArray[1];
            c.setRentCountPerMonth(count);
            bookedDtoList.add(c);
        }
        return bookedDtoList;
    }
    public List<CarCategoryDto> getOnRentCarCategoriesByDate(int year, int month){
                List resultList = entityManager.createQuery(
                "SELECT cc, count(cc) FROM CarCategory cc JOIN Rent r on cc.id = r.carCategoryId where r.status = 'RENT'"
                + "                       AND ( YEAR(r.rentStart) = :y and MONTH(r.rentStart) = :m"
                + "                       OR YEAR(r.rentEnd) = :y and MONTH(r.rentEnd) = :m ) GROUP BY r.carCategoryId")
                .setParameter("y", year)
                .setParameter("m", month)
                .getResultList();
        List<CarCategoryDto> rentDtoList = new ArrayList<>();
        for (Object object : resultList) {
           Object[] objArray = (Object[]) object;  
           CarCategory cc = (CarCategory) objArray[0];
           CarCategoryDto c = new CarCategoryDto(cc.getId(), cc.getName(), cc.getPrice(), cc.getDescription(), cc.getPhoto());
            Long count = (Long) objArray[1];
            c.setRentCountPerMonth(count);
            rentDtoList.add(c);
        }
        return rentDtoList;
    }
}
