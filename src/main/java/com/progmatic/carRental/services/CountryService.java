/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services;

import com.progmatic.carRental.entities.Country;
import com.progmatic.carRental.repositories.CountryRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Molnár Richárd
 */
@Service
public class CountryService {
    
    @Autowired
    CountryRepository countryRepository;
    public List<Country> findAll(){
        
       return countryRepository.findAll();
        
    }
}
