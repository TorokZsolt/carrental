/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services;

import com.progmatic.carRental.entities.Rent;
import com.progmatic.carRental.repositories.RentRepository;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 *
 * @author User
 */

@Service
public class RentService {
    
    @Autowired
    RentRepository rentRepository;
    
    @PersistenceContext
    EntityManager entityManager;
    
    public List<Rent> findLiveRentsList() {
        List resultList = entityManager.createQuery("select r from Rent r "
                + "WHERE status='BOOKED' OR status='RENT'")
                .getResultList();
        return resultList;
    }
    
    public List<Rent> findLiveAndCancelledRentsList() {
        List resultList = entityManager.createQuery("select r from Rent r "
                + "WHERE status='BOOKED' OR status='RENT' OR status='CANCELLED'")
                .getResultList();
        return resultList;
    }
    
    public List<Rent> findLiveAndFinishedRentsList() {
        List resultList = entityManager.createQuery("select r from Rent r "
                + "WHERE status='BOOKED' OR status='RENT' OR status='FINISHED'")
                .getResultList();
        return resultList;
    }
    
    public List<Rent> findAllRentList() {
        List resultList = entityManager.createQuery("select r from Rent r ")
                .getResultList();
        return resultList;
    }
    
    public Rent FindARentByID(int id) {
        Query query = entityManager.createQuery("SELECT r FROM Rent r WHERE r.id = :id")
                .setParameter("id", id);
        List<Rent> resultList = query.getResultList();
        return resultList.get(0);
    }
    
    public void save(Rent r) {
        rentRepository.save(r);
    }
    
    public List<Rent> findFinishedRents() {
        List resultList = entityManager.createQuery("select r from Rent r "
                + "WHERE status='FINISHED'")
                .getResultList();
        return resultList;
    }
}
