/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.progmatic.carRental.entities.Customer;
import com.progmatic.carRental.repositories.CustomerRepository;
import com.progmatic.carRental.services.AutoDao.CustomerAutoDao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author NoDi
 */
@Service
public class CustomerService {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    CustomerRepository customerRepository;

    public void addCustomer(Customer customer) {
//        entityManager.persist(customer);
        customerRepository.save(customer);
    }

    public List<Customer> findCustomer() {
        List resultList = entityManager.createQuery("select m from Customer m")
                .getResultList();
        return resultList;
    }

    public Customer findCustomerById(int id) {
        Customer customerById = customerRepository.findById(id);
        return customerById;
    }
    
//    public Customer findCustomerByEmail(String email) {
//        Customer customerByEmail = customerRepository.findByEmail(email);
//        return customerByEmail;
//    }
    
    
    public Customer findCustomerByEmail(String email){
        Customer result = (Customer) entityManager.createQuery("select c from Customer c where c.email = :email")
                .setParameter("email", email)
                .getSingleResult();
        return result;
    }

    public void save(Customer c) {
        customerRepository.save(c);
    }

}
