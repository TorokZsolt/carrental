/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services;

import com.progmatic.carRental.entities.User;
import com.progmatic.carRental.repositories.UserRepository;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Zsolt
 */
@Service
public class UserService {
    
    @Autowired
    UserRepository userRepository;

    @PersistenceContext
    EntityManager entityManager;
    

    @Transactional
    public void addUser(User user) {
        entityManager.persist(user);
    }

    public List<User> findUser() {
        List resultList = entityManager.createQuery("SELECT m FROM User m")
                .getResultList();
        return resultList;
    }
    
    public User FindUserById(int id) {
        Query query = entityManager.createQuery("SELECT u FROM User u WHERE u.id = :id")
                .setParameter("id", id);
        List<User> resultList = query.getResultList();
        return resultList.get(0);
    }

    public void save(User user) {
        userRepository.save(user);
    }
    
    public User findById(Integer id){
        return userRepository.findById(id).get();
    }
    
    public User findByUserName(String name){
        User user = (User) entityManager.createQuery("SELECT u FROM User u WHERE u.username = :name")
                .setParameter("name", name)
                .getSingleResult();
        return user;
    }
    
}
