/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services;

import com.progmatic.carRental.dto.InvoiceDto;
import com.progmatic.carRental.entities.Invoice;
import com.progmatic.carRental.repositories.InvoiceRepository;
import java.io.ByteArrayInputStream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.logging.Level;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 *
 * @author benko
 */

@Service
public class InvoiceService {
    
    private static final Logger LOG = LoggerFactory.getLogger(InvoiceService.class);

    @Autowired
    InvoiceRepository invoiceRepository;

    
    @PersistenceContext
    EntityManager entityManager;
    
    public Invoice findInvoiceByNumber(String invoiceNumber){
        return invoiceRepository.findByInvoiceNumber(invoiceNumber);
    }

    public void saveTheInvoice(Invoice invoice) {
        invoiceRepository.save(invoice);
    }

    public byte[] sendWithHttps(File xmlFile, Invoice invoice) throws Exception {
        LOG.debug("sendWithHttps started");
        TrustStrategy ts = new TrustAllStrategy();
        SSLConnectionSocketFactory socketFactory
                = new SSLConnectionSocketFactory(
                        new SSLContextBuilder().loadTrustMaterial(ts).build());

        CloseableHttpClient httpClient
                = HttpClients.custom()
                        .setSSLHostnameVerifier(new NoopHostnameVerifier())
                        .setSSLSocketFactory(socketFactory)
                        .build();
        HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();

        FileSystemResource fs = new FileSystemResource(xmlFile);
        parts.add("action-xmlagentxmlfile", fs);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        org.springframework.http.HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new org.springframework.http.HttpEntity(parts, headers);

        RestTemplate restTemplate = new RestTemplate(requestFactory);
        
        ResponseEntity<String> response
                = restTemplate.exchange("https://www.szamlazz.hu/szamla/",
                        HttpMethod.POST, requestEntity, String.class);
        // XML kinyerése a response-bol
        String xmlResponse = response.getBody();
        // PDF kinyerése a kapott XML-bol
        String baes64EncodedPdf = getBaes64EncodedPdf(xmlResponse);
        LOG.debug("base64 encoded string is:  {}", baes64EncodedPdf);
        // Számla szám kinyerése és beállítása
        invoice.setInvoiceNumber(getInvoiceNumber(xmlResponse));
        // PDF Dekodolása
        LOG.debug("gonna decode encoded string");
        byte[] decodedPdf = Base64.getDecoder().decode(baes64EncodedPdf.getBytes(StandardCharsets.UTF_8));
        return decodedPdf;
    }
    
    
    private String getBaes64EncodedPdf(String xml) throws Exception{
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new ByteArrayInputStream(xml.getBytes("UTF-8")));
        NodeList pdfNodes = doc.getElementsByTagName("pdf");
        String textContent = pdfNodes.item(0).getFirstChild().getTextContent();
        return textContent.replaceAll("\n", "");
    }
    
    private String getInvoiceNumber(String xml) throws Exception{
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new ByteArrayInputStream(xml.getBytes("UTF-8")));
        NodeList pdfNodes = doc.getElementsByTagName("szamlaszam");
        String textContent = pdfNodes.item(0).getFirstChild().getTextContent();
        return textContent;
    }
    
    public Long findMaxInvoiceNumber(){
        Long result = (Long) entityManager.createQuery("select m.invoiceNumber from Invoice m order by m.invoiceNumber desc").setMaxResults(1).getSingleResult();
        return result;
    }
    
    public byte[] getInvoicePdf(int pdfId) {
        Invoice invoice = invoiceRepository.findById(pdfId);
        if (invoice != null) {
        return invoice.getInvoicePdf();
        }
        return new byte[]{};
    }
    
}
