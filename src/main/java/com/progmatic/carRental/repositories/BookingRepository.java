/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.repositories;

import com.progmatic.carRental.entities.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author User
 */
public interface BookingRepository extends JpaRepository<Booking, Integer>{
    public Booking findByFirstnameAndLastname(String firstname, String lastname);
    public Booking findById(int numb);
}
