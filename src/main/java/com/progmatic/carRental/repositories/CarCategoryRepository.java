/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.repositories;

import com.progmatic.carRental.entities.CarCategory;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author User
 */
public interface CarCategoryRepository extends JpaRepository<CarCategory, Integer>{
    public CarCategory findByName(String name);
    public CarCategory findById(int numb);
}
