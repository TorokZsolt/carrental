/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.repositories;

import com.progmatic.carRental.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author User
 */
public interface CustomerRepository extends JpaRepository<Customer, Integer>{
    public Customer findById(int numb);
//    public Customer findByEmail(String email);
}
