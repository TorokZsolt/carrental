/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.repositories;

import com.progmatic.carRental.entities.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author User
 */
public interface InvoiceRepository extends JpaRepository<Invoice, Integer>{
    public Invoice findById(int numb);

    public Invoice findByInvoiceNumber(String invoiceNumber);
}
