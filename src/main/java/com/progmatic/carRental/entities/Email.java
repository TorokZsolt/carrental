/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.entities;

import java.util.Date;

/**
 *
 * @author User
 */
public class Email {

    private final String lastName;
    private final String firstName;
    private final Date bookStart;
    private final Date bookEnd;
    private final String placeStart;
    private final String placeEnd;
    private final Car carId;
    private final String hashCode;

    //private final java.sql.Timestamp thisDate;
    public Email(String lastName, String firstName, Date bookStart, Date bookEnd,
            String placeStart, String placeEnd, Car carId, String hashCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        //thisDate = java.sql.Timestamp.valueOf("2007-09-23 10:10:10");
        this.bookStart = bookStart;
        this.bookEnd = bookEnd;
        this.placeStart = placeStart;
        this.placeEnd = placeEnd;
        this.carId = carId;
        this.hashCode = hashCode;
    }

    public String getHash() {
        return hashCode;
    }

    public int getCarUnitPrice() {
        return carId.getCarCategoryId().getPrice();
    }

    public int getCarFullDaysPrice() {
        if ((bookEnd.getDay() - bookStart.getDay()) * carId.getCarCategoryId().getPrice() != 0) {
            return (bookEnd.getDay() - bookStart.getDay()) * carId.getCarCategoryId().getPrice();
        } else {
            return carId.getCarCategoryId().getPrice();
        }

    }

    public String getCarType() {
        return carId.getType();
    }

    public String getResumptionPlace() {
        return placeEnd;
    }

    public String getReceivePlace() {
        return placeStart;
    }

    public Date getEndDate() {
        return bookEnd;
    }

    public Date getStartDate() {
        return bookStart;
    }

    public String getName() {
        String name = lastName + " " + firstName;
        return name;
    }

//    public java.sql.Timestamp ThisDate() {
//        return thisDate; 
//    }
}
