/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.entities;

import com.progmatic.carRental.enums.RentStatusEnum;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author NoDi
 */
@Entity
@Table(name = "rent")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rent.findAll", query = "SELECT r FROM Rent r")
    , @NamedQuery(name = "Rent.findById", query = "SELECT r FROM Rent r WHERE r.id = :id")
    , @NamedQuery(name = "Rent.findByRentStart", query = "SELECT r FROM Rent r WHERE r.rentStart = :rentStart")
    , @NamedQuery(name = "Rent.findByRentEnd", query = "SELECT r FROM Rent r WHERE r.rentEnd = :rentEnd")
    , @NamedQuery(name = "Rent.findByStatus", query = "SELECT r FROM Rent r WHERE r.status = :status")
    , @NamedQuery(name = "Rent.findByKmStart", query = "SELECT r FROM Rent r WHERE r.kmStart = :kmStart")
    , @NamedQuery(name = "Rent.findByKmEnd", query = "SELECT r FROM Rent r WHERE r.kmEnd = :kmEnd")
    , @NamedQuery(name = "Rent.findByFuelLevelStart", query = "SELECT r FROM Rent r WHERE r.fuelLevelStart = :fuelLevelStart")
    , @NamedQuery(name = "Rent.findByFuelLevelEnd", query = "SELECT r FROM Rent r WHERE r.fuelLevelEnd = :fuelLevelEnd")
    , @NamedQuery(name = "Rent.findByPlaceStart", query = "SELECT r FROM Rent r WHERE r.placeStart = :placeStart")
    , @NamedQuery(name = "Rent.findByPlaceEnd", query = "SELECT r FROM Rent r WHERE r.placeEnd = :placeEnd")
    , @NamedQuery(name = "Rent.findByOfferPrice", query = "SELECT r FROM Rent r WHERE r.offerPrice = :offerPrice")
    , @NamedQuery(name = "Rent.findByFinalPrice", query = "SELECT r FROM Rent r WHERE r.finalPrice = :finalPrice")
    , @NamedQuery(name = "Rent.findByHash", query = "SELECT r FROM Rent r WHERE r.hash = :hash")})
public class Rent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rent_start")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date rentStart;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rent_end")
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date rentEnd;
    @Basic(optional = true)
    @NotNull
    @Column(name = "status")
    private String status;
    @Basic(optional = true)
//    @NotNull
    @Column(name = "km_start")
    private int kmStart;
    @Basic(optional = true)
//    @NotNull
    @Column(name = "km_end")
    private int kmEnd;
    @Basic(optional = true)
//    @NotNull
    @Column(name = "fuel_level_start")
    private short fuelLevelStart;
    @Basic(optional = true)
//    @NotNull
    @Column(name = "fuel_level_end")
    private short fuelLevelEnd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "place_start")
    private String placeStart;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "place_end")
    private String placeEnd;
    @Basic(optional = true)
    @NotNull
    @Column(name = "offer_price")
    private int offerPrice;
    @Basic(optional = true)
    @NotNull
    @Column(name = "final_price")
    private int finalPrice;
    @Basic(optional = true)
//    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "hash", unique = true)
    private String hash;
    @JoinColumn(name = "car_id", referencedColumnName = "id")
    @ManyToOne
    private Car carId;
    @JoinColumn(name = "car_category_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CarCategory carCategoryId;
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Customer customerId;
    @JoinColumn(name = "invoice_id", referencedColumnName = "id")
    @ManyToOne(optional = true/*, cascade = CascadeType.ALL*/) //Hogyha mentődik ez akkor mentsen minden hozzátartozót
    private Invoice invoiceId;
    @JoinColumn(name = "extra_invoice_id", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private Invoice extraInvoiceId;
    @JoinColumn(name = "start_user_id", referencedColumnName = "id")
    @ManyToOne/*(optional = false)*/
    private User startUserId;
    @JoinColumn(name = "end_user_id", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private User endUserId;

    public Rent() {
    }

    public Rent(Date rentStart, Date rentEnd, RentStatusEnum status, int kmStart,
            int kmEnd, short fuelLevelStart, short fuelLevelEnd, String placeStart,
            String placeEnd, int offerPrice, int finalPrice, String hash) {
        this.rentStart = rentStart;
        this.rentEnd = rentEnd;
        this.status = status.name();
        this.kmStart = kmStart;
        this.kmEnd = kmEnd;
        this.fuelLevelStart = fuelLevelStart;
        this.fuelLevelEnd = fuelLevelEnd;
        this.placeStart = placeStart;
        this.placeEnd = placeEnd;
        this.offerPrice = offerPrice;
        this.finalPrice = finalPrice;
        this.hash = hash;
    }

    public String getFullName() {
        String name = this.customerId.getLastname() + " " + this.customerId.getFirstname();
        return name;
    }
    
    public int getRentingDay() {
        Date date2;
        if (this.rentEnd.before(new Date())) {
            date2 = new Date();
        } else {
            date2 = this.rentEnd;
        }
        LocalDate startDate = this.rentStart.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = this.rentEnd.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return startDate.until(endDate).getDays();
    }

    public Integer getId() {
        return id;
    }

    public Date getRentStart() {
        return rentStart;
    }

    public void setRentStart(Date rentStart) {
        this.rentStart = rentStart;
    }

    public Date getRentEnd() {
        return rentEnd;
    }

    public void setRentEnd(Date rentEnd) {
        this.rentEnd = rentEnd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(RentStatusEnum status) {
        this.status = status.name();
    }

    public int getKmStart() {
        return kmStart;
    }

    public void setKmStart(int kmStart) {
        this.kmStart = kmStart;
    }

    public int getKmEnd() {
        return kmEnd;
    }

    public void setKmEnd(int kmEnd) {
        this.kmEnd = kmEnd;
    }

    public short getFuelLevelStart() {
        return fuelLevelStart;
    }

    public void setFuelLevelStart(short fuelLevelStart) {
        this.fuelLevelStart = fuelLevelStart;
    }

    public short getFuelLevelEnd() {
        return fuelLevelEnd;
    }

    public void setFuelLevelEnd(short fuelLevelEnd) {
        this.fuelLevelEnd = fuelLevelEnd;
    }

    public String getPlaceStart() {
        return placeStart;
    }

    public void setPlaceStart(String placeStart) {
        this.placeStart = placeStart;
    }

    public String getPlaceEnd() {
        return placeEnd;
    }

    public void setPlaceEnd(String placeEnd) {
        this.placeEnd = placeEnd;
    }

    public int getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(int offerPrice) {
        this.offerPrice = offerPrice;
    }

    public int getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(int finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Car getCarId() {
        return carId;
    }

    public void setCarId(Car carId) {
        this.carId = carId;
    }

    public CarCategory getCarCategoryId() {
        return carCategoryId;
    }

    public void setCarCategoryId(CarCategory carCategoryId) {
        this.carCategoryId = carCategoryId;
    }

    public Customer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customer customerId) {
        this.customerId = customerId;
    }

    public Invoice getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Invoice invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Invoice getExtraInvoiceId() {
        return extraInvoiceId;
    }

    public void setExtraInvoiceId(Invoice extraInvoiceId) {
        this.extraInvoiceId = extraInvoiceId;
    }

    public User getStartUserId() {
        return startUserId;
    }

    public void setStartUserId(User startUserId) {
        this.startUserId = startUserId;
    }

    public User getEndUserId() {
        return endUserId;
    }

    public void setEndUserId(User endUserId) {
        this.endUserId = endUserId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rent)) {
            return false;
        }
        Rent other = (Rent) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.progmatic.carRental.Rent[ id=" + id + " ]";
    }

}
