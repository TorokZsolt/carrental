/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.entities;

import com.progmatic.carRental.enums.IdCardTypeEnum;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author NoDi
 */
@Entity
@Table(name = "customer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Customer.findAll", query = "SELECT c FROM Customer c")
    , @NamedQuery(name = "Customer.findById", query = "SELECT c FROM Customer c WHERE c.id = :id")
    , @NamedQuery(name = "Customer.findByFirstname", query = "SELECT c FROM Customer c WHERE c.firstname = :firstname")
    , @NamedQuery(name = "Customer.findByLastname", query = "SELECT c FROM Customer c WHERE c.lastname = :lastname")
    , @NamedQuery(name = "Customer.findByAddressZip", query = "SELECT c FROM Customer c WHERE c.addressZip = :addressZip")
    , @NamedQuery(name = "Customer.findByAddressCountryId", query = "SELECT c FROM Customer c WHERE c.addressCountryId = :addressCountryId")
    , @NamedQuery(name = "Customer.findByAddressCity", query = "SELECT c FROM Customer c WHERE c.addressCity = :addressCity")
    , @NamedQuery(name = "Customer.findByAddressStreet", query = "SELECT c FROM Customer c WHERE c.addressStreet = :addressStreet")
    , @NamedQuery(name = "Customer.findByDrivingLicence", query = "SELECT c FROM Customer c WHERE c.drivingLicence = :drivingLicence")
    , @NamedQuery(name = "Customer.findByIdCardType", query = "SELECT c FROM Customer c WHERE c.idCardType = :idCardType")
    , @NamedQuery(name = "Customer.findByIdCardNumber", query = "SELECT c FROM Customer c WHERE c.idCardNumber = :idCardNumber")
    , @NamedQuery(name = "Customer.findByDrivingLicenceValid", query = "SELECT c FROM Customer c WHERE c.drivingLicenceValid = :drivingLicenceValid")
    , @NamedQuery(name = "Customer.findByEmail", query = "SELECT c FROM Customer c WHERE c.email = :email")
    , @NamedQuery(name = "Customer.findByPhone", query = "SELECT c FROM Customer c WHERE c.phone = :phone")
    , @NamedQuery(name = "Customer.findByBirthDate", query = "SELECT c FROM Customer c WHERE c.birthDate = :birthDate")})
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "firstname")
    private String firstname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "lastname")
    private String lastname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "address_zip")
    private String addressZip;
    @Basic(optional = false)
    @NotNull
    @JoinColumn(name = "address_country_id")
    @ManyToOne(optional = false)
    private Country addressCountryId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "address_city")
    private String addressCity;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "address_street")
    private String addressStreet;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "driving_licence")
    private String drivingLicence;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_card_type")
    private String idCardType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "id_card_number")
    private String idCardNumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "driving_licence_valid")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date drivingLicenceValid;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "email", unique = true)
    @Email
    private String email;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "phone")
    private String phone;
    @Column(name = "birth_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerId")
    private Collection<Rent> rentCollection;

    public Customer() {
    }

    public Customer(String firstname, String lastname, String addressZip, Country addressCountryId, String addressCity, String addressStreet, String drivingLicence, IdCardTypeEnum idCardType, String idCardNumber, Date drivingLicenceValid, String email, String phone) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.addressZip = addressZip;
        this.addressCountryId = addressCountryId;
        this.addressCity = addressCity;
        this.addressStreet = addressStreet;
        this.drivingLicence = drivingLicence;
        this.idCardType = idCardType.name();
        this.idCardNumber = idCardNumber;
        this.drivingLicenceValid = drivingLicenceValid;
        this.email = email;
        this.phone = phone;
    }
    
//    public Customer(int id, String firstname, String lastname, String email){
//        this.id = id;
//        this.firstname = firstname;
//        this.lastname = lastname;
//        this.email = email;
//    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddressZip() {
        return addressZip;
    }

    public void setAddressZip(String addressZip) {
        this.addressZip = addressZip;
    }

    public Country getAddressCountryId() {
        return addressCountryId;
    }

    public void setAddressCountryId(Country addressCountryId) {
        this.addressCountryId = addressCountryId;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public String getDrivingLicence() {
        return drivingLicence;
    }

    public void setDrivingLicence(String drivingLicence) {
        this.drivingLicence = drivingLicence;
    }

    public String getIdCardType() {
        return idCardType;
    }

    public void setIdCardType(String idCardType) {
        this.idCardType = idCardType;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    public Date getDrivingLicenceValid() {
        return drivingLicenceValid;
    }

    public void setDrivingLicenceValid(Date drivingLicenceValid) {
        this.drivingLicenceValid = drivingLicenceValid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @XmlTransient
    public Collection<Rent> getRentCollection() {
        return rentCollection;
    }

    public void setRentCollection(Collection<Rent> rentCollection) {
        this.rentCollection = rentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customer)) {
            return false;
        }
        Customer other = (Customer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.progmatic.carRental.Customer[ id=" + id + " ]";
    }
    
}
