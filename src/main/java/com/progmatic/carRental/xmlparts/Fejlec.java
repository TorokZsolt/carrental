package com.progmatic.carRental.xmlparts;

import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author User
 */

    // A Számlázz.hu minta xml-nek megfelelő fieldek kellenek!
    // <fejlec> blokk

public class Fejlec {
    
    @XmlElement
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    private Date keltDatum;                         // minta 2012-04-12
    @XmlElement
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    private Date teljesitesDatum;                   // minta 2012-04-10
    @XmlElement
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    private Date fizetesiHataridoDatum;             // minta 2012-04-20
    @XmlElement
    private static String fizmod = "Készpénz";   
    @XmlElement
    private static String penznem = "Ft";        
    @XmlElement
    private String szamlaNyelve = "hu";          
    @XmlElement
    private String megjegyzes;                      //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    private String arfolyamBank = "MNB";            //Ha az arfolyamBank MNB és nincs megadva az arfolyam, akkor az MNB aktuális árfolyamát használjuk a számlakészítéskor)
    @XmlElement
    private double arfolyam = 0.0;                  
    @XmlElement
    private String rendelesSzam;                    //előlegszámlához kell megadni
    @XmlElement
    private boolean elolegszamla = true;            //ezt használjuk!
    @XmlElement
    private boolean vegszamla = false;              //ezt csak úgy lehetne, ha már létezik előlegszámla az adott rendelésszámmal
    @XmlElement
    private boolean helyesbitoszamla = false;           
    @XmlElement
    private boolean dijbekero = false;                  
    @XmlElement
    private boolean szallitolevel = false;              
    @XmlElement
    private String szamlaszamElotag = "PRG";        //Számlázz.hu generálta, az online felületen fent van
    @XmlElement
    private boolean fizetve = true;                 //a számla elkészítését követően azonnal kifizetettként adminisztrálódik
    
    public Fejlec(){
        
    }

    public Fejlec(Date keltDatum, Date teljesitesDatum, Date fizetesiHataridoDatum, String megjegyzes, String rendelesSzam) {
        this.keltDatum = keltDatum;
        this.teljesitesDatum = teljesitesDatum;
        this.fizetesiHataridoDatum = fizetesiHataridoDatum;
        this.megjegyzes = megjegyzes;
        this.rendelesSzam = rendelesSzam;
    }

    // getter nem lehet (mert duplikációt jelez a generálás során)
    // setter nem szükséges, mert konstruktorban adjuk át az adatokat, illetve be vannak égetve
    
}
