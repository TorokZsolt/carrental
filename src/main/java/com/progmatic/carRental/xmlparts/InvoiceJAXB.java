package com.progmatic.carRental.xmlparts;

import java.io.File;
import java.util.Date;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.springframework.stereotype.Service;

/**
 *
 * @author User
 */
            // Csak teszt osztály (service/controllerbe lesz majd)

@Service
public class InvoiceJAXB {
    
    public static File createXml(Date keltDatum, Date teljesitesDatum, Date fizetesiHataridoDatum,
            String megjegyzes, String rendelesSzam, 
            String nev, int irSzam, String telepules, String cim, String adoszam,  
            String megnevezes, int menny, int netEgysAr, int netErtek, int afaErtek,
            int bruttoErtek, String megjegyzesTetel) {
        
//        Xmlszamla xmlSzamla = testDataWithConstructor(new Date(118,8,10), new Date(118,8,8),
//                new Date(118,8,10), "Helyszíni fizetés.", "1",
//                "Vevő Miksa", 1113, "Budapest", "Teszt u. 1.", "12345678-1-43", 
//                "Renault Clio", 1, 10000, 10000, 2700, 12700, "Kölcsönzési szolgáltatás díj.");
        Xmlszamla xmlSzamla = new Xmlszamla(keltDatum, teljesitesDatum, fizetesiHataridoDatum,
            megjegyzes, rendelesSzam, nev, irSzam, telepules, cim, adoszam,  
            megnevezes, menny, netEgysAr, netErtek, afaErtek, bruttoErtek, megjegyzesTetel);
        // XML generálás
        return jaxbObjectToXML(xmlSzamla);
    }

    private static File jaxbObjectToXML(Xmlszamla xmlSzamla) {
        File file = new File("C:\\Users\\User\\Desktop\\inoviceTeszt.xml");
        try { 
            //\gitProjects\\rent_a_car\\carRental\\invoices\\
            JAXBContext jaxbContext = JAXBContext.newInstance(Xmlszamla.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            jaxbMarshaller.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, "http://www.szamlazz.hu/xmlszamla http://www.szamlazz.hu/docs/xsds/agent/xmlszamla.xsd");
            jaxbMarshaller.marshal(xmlSzamla, file);        // fájlba írás
            //jaxbMarshaller.marshal(xmlSzamla, System.out);  // ha a System outra is ki akarjuk íratni a generált xml-t
            
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return file;
    }
    
    private static Xmlszamla testDataWithConstructor(
            Date keltDatum, Date teljesitesDatum, Date fizetesiHataridoDatum,
            String megjegyzes, String rendelesSzam, 
            String nev, int irSzam, String telepules, String cim, String adoszam,  
            String megnevezes, int menny, int netEgysAr, int netErtek, int afaErtek,
            int bruttoErtek, String megjegyzesTetel){
        
        Xmlszamla xmlSzamla = new Xmlszamla(keltDatum, teljesitesDatum, fizetesiHataridoDatum, megjegyzes, rendelesSzam
                , nev, irSzam, telepules, cim, adoszam,  
                megnevezes, menny, netEgysAr, netErtek, afaErtek, bruttoErtek, megjegyzesTetel);
        return xmlSzamla;
    }

}
