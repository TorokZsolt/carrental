package com.progmatic.carRental.xmlparts;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author User
 */

    // A Számlázz.hu minta xml-nek megfelelő fieldek kellenek!
    // <tetelek> blokk

public class Tetelek {
    
    @XmlElement
    public Tetel tetel;

    //Setter beállítással
    public Tetelek() {
        tetel = new Tetel();
    }
    
    //Konstruktor beállítással
    public Tetelek(String megnevezes, double mennyiseg, int nettoEgysegar, double nettoErtek, double afaErtek, double bruttoErtek, 
            String megjegyzes){
        tetel = new Tetel(megnevezes, mennyiseg, nettoEgysegar, nettoErtek, afaErtek, bruttoErtek, megjegyzes);
    }

    public Tetelek(Tetel tetel) {
        this.tetel = tetel;
    }

    // getter nem lehet (mert duplikációt jelez a generálás során)
    
    public void setTetel(Tetel tetel) {
        this.tetel = tetel;
    }
    
}
