package com.progmatic.carRental.xmlparts;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author User
 */

    // A Számlázz.hu minta xml-nek megfelelő fieldek kellenek!
    // <tetel> blokk

public class Tetel {
    
    @XmlElement
    private String megnevezes;
    @XmlElement
    private double mennyiseg;               
    @XmlElement
    private String mennyisegiEgyseg = "nap";  
    @XmlElement
    private double nettoEgysegar;              
    @XmlElement
    private static String afakulcs = "27";       // ua. adható meg, mint a számlakészítés oldalon
    @XmlElement
    private double nettoErtek;              
    @XmlElement
    private double afaErtek;                
    @XmlElement
    private double bruttoErtek;             
    @XmlElement
    private String megjegyzes;     
//    @XmlElement
//    public TetelFokonyv tetelFokonyv;         //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)

    public Tetel() {
        
    }

    public Tetel(String megnevezes, double mennyiseg, int nettoEgysegar, double nettoErtek, double afaErtek, double bruttoErtek, 
            String megjegyzes) {
        this.megnevezes = megnevezes;
        this.mennyiseg = mennyiseg;
        this.nettoEgysegar = nettoEgysegar;
        this.nettoErtek = nettoErtek;
        this.afaErtek = afaErtek;
        this.bruttoErtek = bruttoErtek;
        this.megjegyzes = megjegyzes;
        //tetelFokonyv = new TetelFokonyv();  //jelenleg be vannak égetve az üres adatok (azért nincs bejövő paraméter)
    }

    // getter nem lehet (mert duplikációt jelez a generálás során)
    // setter nem szükséges, mert konstruktorban adjuk át az adatokat, illetve be vannak égetve

}
