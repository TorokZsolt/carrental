package com.progmatic.carRental.xmlparts;

import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author User
 */
@XmlRootElement
@XmlType(propOrder = { "beallitasok", "fejlec", "elado", "vevo", "tetelek" })
public class Xmlszamla {

    // A Számlázz.hu minta xml-nek megfelelő blokk sorrend!
    @XmlElement
    Beallitasok beallitasok;    
    @XmlElement
    Fejlec fejlec;              
    @XmlElement
    Elado elado;
    @XmlElement
    Vevo vevo;
    @XmlTransient               // hogy ne látszódjon, a fuvarlevél nem kötelező szekció (nem is valószínű, hogy használjuk)
    Fuvarlevel fuvarlevel;      
    @XmlElement
    Tetelek tetelek;

    public Xmlszamla() {
    }
    
    public Xmlszamla(Date keltDatum, Date teljesitesDatum, Date fizetesiHataridoDatum, String megjegyzes, String rendelesSzam,
            String nev, int irsz, String telepules, String cim, String adoszam,
            String megnevezes, double mennyiseg, int nettoEgysegar, double nettoErtek, double afaErtek, double bruttoErtek, String megjegyzesTetel) {
        beallitasok = new Beallitasok(); //adatok beégetve
        fejlec = new Fejlec(keltDatum, teljesitesDatum, fizetesiHataridoDatum, megjegyzes, rendelesSzam);
        elado = new Elado(); //adatok beégetve
        vevo = new Vevo(nev, irsz, telepules, cim, adoszam);
        tetelek = new Tetelek(megnevezes, mennyiseg, nettoEgysegar, nettoErtek, afaErtek, bruttoErtek, megjegyzesTetel);
    }

    // getter nem lehet (mert duplikációt jelez a generálás során)
    // setter nem szükséges, mert konstruktorban adjuk át az adatokat, illetve be vannak égetve

}
