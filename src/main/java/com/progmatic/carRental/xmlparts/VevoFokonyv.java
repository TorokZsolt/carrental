package com.progmatic.carRental.xmlparts;

import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author User
 */

    // A Számlázz.hu minta xml-nek megfelelő fieldek kellenek!
    // <vevoFokonyv> blokk

public class VevoFokonyv {
    
    @XmlElement
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    private Date konyvelesDatum;                    //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    private int vevoAzonosito;                      //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    private int vevoFokonyviSzam;                   //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    private boolean folyamatosTelj = false;         //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)       

    public VevoFokonyv() {
    }

    public VevoFokonyv(Date konyvelesDatum, int vevoAzonosito, int vevoFokonyviSzam) {
        this.konyvelesDatum = konyvelesDatum;
        this.vevoAzonosito = vevoAzonosito;
        this.vevoFokonyviSzam = vevoFokonyviSzam;
    }

    // getter nem lehet (mert duplikációt jelez a generálás során)
    // setter nem szükséges, mert konstruktorban adjuk át az adatokat, illetve be vannak égetve
    
}
