package com.progmatic.carRental.controller;

//import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.progmatic.carRental.dto.RentAndCustomer;
import com.progmatic.carRental.entities.Booking;
import com.progmatic.carRental.entities.CarCategory;
import com.progmatic.carRental.entities.Customer;
import com.progmatic.carRental.entities.Invoice;
import com.progmatic.carRental.entities.Rent;
import com.progmatic.carRental.entities.UnrentedCarForm;
import com.progmatic.carRental.entities.User;
import com.progmatic.carRental.enums.RentStatusEnum;
import com.progmatic.carRental.repositories.BookingRepository;
import com.progmatic.carRental.services.BookingService;
import com.progmatic.carRental.services.CarCategoryService;
import com.progmatic.carRental.services.CountryService;
import com.progmatic.carRental.services.CustomerService;
import com.progmatic.carRental.services.InvoiceService;
import com.progmatic.carRental.services.RecordNewRentServices;
import com.progmatic.carRental.services.UserService;
import java.security.Principal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 *
 * @author Jani
 */

@Controller
public class RecordNewRentController {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    RecordNewRentServices recordNewRentServices;

    @Autowired
    CustomerService customerservices;

    @Autowired
    CountryService countryservice;

    @Autowired
    CarCategoryService carCategoryService;

    @Autowired
    UserService userService;

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    BookingService bookingService;
    
    @Autowired
    BookingRepository bookingRepository;

    @RequestMapping(value = {"/recordNewRent"}, method = RequestMethod.GET)
    public String showNewRentPage(Model model,HttpServletRequest hsr) {
        Customer customer = new Customer();
        Rent rent = new Rent();

        RentAndCustomer rac = new  RentAndCustomer();
        rac.setRent(rent);
        rac.setCustomer(customer);


        model.addAttribute("rentAndCustomer", rac);
        model.addAttribute("countries", countryservice.findAll());
        model.addAttribute("carCategory", carCategoryService.findAll());
        return "recordNewRent";
    }

    @RequestMapping(value = {"/recordNewRent/{bookingId}"}, method = RequestMethod.GET)
    public String showNewRentPage(
            @PathVariable("bookingId") int bookingId,
            Model model) {

        Customer customer = new Customer();
        Rent rent = new Rent();

        Booking booking = bookingService.findBookingById(bookingId);

        rent = setRentByBooking(booking, rent);
        customer = setCustomerByBooking(booking, customer);

        RentAndCustomer rac = new  RentAndCustomer();
        rac.setRent(rent);
        rac.setCustomer(customer);
        rac.setCarCatId(booking.getCarId().getCarCategoryId().getId());
        rac.setBooking(booking.getId());

        model.addAttribute("rentAndCustomer", rac);
        model.addAttribute("countries", countryservice.findAll());
        model.addAttribute("carCategory", carCategoryService.findAll());

        return "recordNewRent";
    }

    @RequestMapping(path = "/recordNewRent", method = RequestMethod.POST)
    public String createNewRent(@Valid RentAndCustomer rac, BindingResult results, Model model, Principal principal) {
        if(results.hasErrors()){
            System.out.println("Hiba történt a rentAndCustomer-ben");
            model.addAttribute("countries", countryservice.findAll());
            model.addAttribute("carCategory", carCategoryService.findAll());
            return "recordNewRent";
        }
        rac.getRent().setCustomerId(rac.getCustomer());
        rac.getRent().setCarCategoryId(carCategoryService.findByCategoryId(rac.getCarCatId()));
        Invoice invoice = new Invoice(//invoiceService.findMaxInvoiceNumber()+1,
                rac.getRent().getRentStart(),
                rac.getRent().getRentEnd(),
                getInvoiceAmount(rac.getRent().getRentStart(),rac.getRent().getRentEnd(), rac.getRent().getCarCategoryId()));
        invoiceService.saveTheInvoice(invoice);
        rac.getRent().setOfferPrice(getInvoiceAmount(rac.getRent().getRentStart(),rac.getRent().getRentEnd(), rac.getRent().getCarCategoryId()));
        rac.getRent().setInvoiceId(invoice);
        //--> Find by Logged User
        rac.getRent().setStartUserId(userService.findByUserName(principal.getName()));
        try {
            customerservices.addCustomer(rac.getCustomer());
        } catch (DataIntegrityViolationException e) {
            rac.getRent().setCustomerId(customerservices.findCustomerByEmail(rac.getCustomer().getEmail()));
        }
        recordNewRentServices.addRent(rac.getRent());
//        bookingService.deleteBooked(bookingService.findBookingById(rac.getBooking()));
        if(rac.getBooking()!= null){
            bookingRepository.deleteById(rac.getBooking());
        }
        return "redirect:/liveBookingsAndRentingsList";
    }

    private int getInvoiceAmount(Date start, Date end, CarCategory cc){
        //--> invoice renderelése megadott autókategória és dátum alapján
//        LocalDate startDate = rac.getRent().getRentStart().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//        LocalDate endDate = rac.getRent().getRentEnd().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//        int rentDays = startDate.until(endDate).getDays();
        LocalDate startDate = start.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = end.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int rentDays = startDate.until(endDate).getDays();
        int amount = cc.getPrice()*rentDays;
        return amount;
    }

    private Rent setRentByBooking(Booking booking, Rent rent){
        rent.setCarCategoryId(booking.getCarId().getCarCategoryId());
        rent.setCarId(booking.getCarId());
        rent.setPlaceStart(booking.getPlaceStart());
        rent.setPlaceEnd(booking.getPlaceEnd());
        rent.setHash(booking.getHashCode());
        rent.setRentStart(booking.getBookStart());
        rent.setRentEnd(booking.getBookEnd());
        return rent;
    }

    private Customer setCustomerByBooking(Booking booking, Customer customer) {
        customer.setFirstname(booking.getFirstname());
        customer.setLastname(booking.getLastname());
        customer.setEmail(booking.getEmail());
        customer.setPhone(booking.getPhone());
        return customer;
    }
    
//    private Booking setRentByBooking(Customer customer, Rent rent){
//        Booking booking = new Booking();
//        booking.setCarCategoryId(booking.getCarId().getCarCategoryId());
//        booking.setCarId(booking.getCarId());
//        booking.setPlaceStart(booking.getPlaceStart());
//        booking.setPlaceEnd(booking.getPlaceEnd());
//        booking.setHash(booking.getHashCode());
//        booking.setRentStart(booking.getBookStart());
//        booking.setRentEnd(booking.getBookEnd());
//        return booking;
//    }
       
}
