/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.controller;

import com.progmatic.carRental.entities.Booking;
import com.progmatic.carRental.entities.UnrentedCarForm;
import com.progmatic.carRental.services.BookingService;
import com.progmatic.carRental.services.StoreNoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Kalman
 */
@Controller
public class StoreNoController {

    @Autowired
    StoreNoService storeNoService;

    @Autowired
    BookingService bookingService;

//    @RequestMapping(value = "/storeno", method = RequestMethod.GET)
//    public String carFinderPage(Model model, @ModelAttribute(name = "delete") UnrentedCarForm form) {
//
//        List<Booking> b = bookingService.findBooking();
//        b = storeNoService.bookingHash(""); // ide kell írni a bejövő hasht
//        if (b.isEmpty()) {
//            b = storeNoService.rentHash("");// meg ide is
//        }
//        if (b.isEmpty()) {
//            System.out.println("nem található ilyen kód");
//            // TODO ha üres akkor csináljon valamit és szóljon h ilyen nincs/már törölve
//        }
//        return "storeno"; //és tegye is bele
//    }
    @RequestMapping(value = {"/storno/{hash}"}, method = RequestMethod.GET)
    public String showFormById(
            @PathVariable("hash") String hashCode,
            Model model) {

        Booking booking = storeNoService.FindABookByHash(hashCode);
        storeNoService.deleted(booking);
        return "storno";
    }
}
