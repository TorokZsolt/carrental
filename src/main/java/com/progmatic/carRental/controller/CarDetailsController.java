/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.controller;

import com.progmatic.carRental.entities.Car;
import com.progmatic.carRental.entities.CarCategory;
import com.progmatic.carRental.enums.EngineTypeEnum;
import com.progmatic.carRental.services.CarCategoryService;
import com.progmatic.carRental.services.CarService;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author User
 */
@Controller
public class CarDetailsController {

    @Autowired
    ApplicationContext context;

    @Autowired
    CarService carservice;

    @Autowired
    CarCategoryService carCategoryService;

    @RequestMapping(value = {"/carCategory"}, method = RequestMethod.GET)
    public String showCarCategory(
            @RequestParam(required = false, defaultValue = "10", name = "maxmsg") Integer msgMax,
            Model model) {
        model.addAttribute("carCategory", carservice.findCarCategory(msgMax));
        model.addAttribute("pageTitle", "Car Category");
        return "carCategory";
    }

    //html megjelenitese
    @RequestMapping(value = {"/carDetails"}, method = RequestMethod.GET)
    public String showForm(Model model) {

        Car car = new Car();

        CarCategory selectedCategory = new CarCategory();
        model.addAttribute("selectedCategory", selectedCategory);

        model.addAttribute("categories", carCategoryService.findAll());
        model.addAttribute("car", car);
        model.addAttribute("pageTitle", "Car Details");
        model.addAttribute("engineTypes", carservice.getEngineTypeList());

        return "carDetails";
    }

    @RequestMapping(value = {"/carDetails/{id}"}, method = RequestMethod.GET)
    public String showFormById(
            @PathVariable("id") Integer carId,
            Model model) {

        Car car = carservice.FindACarByID(carId);
        model.addAttribute("car", car);
        CarCategory selectedCategory = car.getCarCategoryId();
        model.addAttribute("selectedCategory", selectedCategory);

        model.addAttribute("categories", carCategoryService.findAll());

        model.addAttribute("engineTypes", carservice.getEngineTypeList());

        EngineTypeEnum selectedEngine = EngineTypeEnum.valueOf(car.getEngineType());
        model.addAttribute("selectedEngine", selectedEngine);

        model.addAttribute("isDeleted", car.getIsDeleted());

        return "carDetails";
    }

    @RequestMapping(value = {"/carDetails"}, method = RequestMethod.POST)
    public String saveTheCar(@Valid Car car, BindingResult results,Model model) {
        if(results.hasErrors()){
            model.addAttribute("engineTypes", carservice.getEngineTypeList()); 
            CarCategory selectedCategory = car.getCarCategoryId();
            model.addAttribute("selectedCategory", selectedCategory);
            model.addAttribute("categories", carCategoryService.findAll());
            return "carDetails";
        }
        carservice.save(car);
        return "redirect:/carList";
    }

    @RequestMapping(value = {"/carList"}, method = RequestMethod.GET)
    public String showCarList(Model model, HttpServletRequest request) {
        if (request.getSession().getAttribute("showDeletedCars") != null &&
                (Boolean) request.getSession().getAttribute("showDeletedCars")) {
            model.addAttribute("carsList", carservice.findAllCarList());
            model.addAttribute("selectDeleted", true);
        } else {
            model.addAttribute("carsList", carservice.findNotDeletedCarList());
            model.addAttribute("selectDeleted", false);
        }
        model.addAttribute("pageTitle", "Car List");
        return "carList";
    }
    
    @RequestMapping(value = {"/carList/setDeletedVisible"}, method = RequestMethod.GET)
    public String carListSetDeletedVisible(
            @RequestParam(required = false, name = "d", defaultValue = "0") Integer deleted,
            Model model, HttpServletRequest request) {
        request.getSession().setAttribute("showDeletedCars", deleted == 1);
        return "redirect:/carList";
    }

}
