/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.controller;

import com.progmatic.carRental.dto.CarDescriptionDto;
import com.progmatic.carRental.entities.Car;
import com.progmatic.carRental.entities.Customer;
import com.progmatic.carRental.entities.Rent;
import com.progmatic.carRental.services.CarService;
import com.progmatic.carRental.services.CounterService;
import com.progmatic.carRental.services.CustomerService;
import com.progmatic.carRental.services.RentService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 *
 * @author NoDi
 */
@Controller
public class IndexHtmlController {

    @Autowired
    CarService carService;

    @Autowired
    CounterService counterService;

    @Autowired
    CustomerService customerService;

    @Autowired
    RentService rentService;

    @RequestMapping(value = {"/home", "/"}, method = GET)
    public String home(Model model) {
        List<Car> randomCars = carService.getRandomCars(14);
        List<Car> countCars = carService.findAllCarList();
        List<Customer> countCustomer = customerService.findCustomer();
        List<Rent> countRent = rentService.findAllRentList();
        model.addAttribute("pageTitle", "Car Rental");
        model.addAttribute("carlist", carService.findAllCarList());
        model.addAttribute("randomCarList", randomCars);
        model.addAttribute("cardescription", getCarDescriptions(randomCars));
        model.addAttribute("priceList", carService.findAllCarPrice());
        model.addAttribute("categoryNameList", carService.findAllCategoryNameList());
        model.addAttribute("counterCars", counterService.countCars(countCars));
        model.addAttribute("counterCustomer", counterService.countCustomer(countCustomer));
        model.addAttribute("counterRent", counterService.countRents(countRent));
        return "index";
    }

    public List<CarDescriptionDto> getCarDescriptions(List<Car> cars) {
        List<CarDescriptionDto> descriptions = new ArrayList<>();
        for (Car car : cars) {
            String desc = car.getCarCategoryId().getDescription();
            String[] splitteddesc = desc.split(",");

            CarDescriptionDto cdd = new CarDescriptionDto(splitteddesc[0].trim(), splitteddesc[1].replace(" Doors", "").trim(), splitteddesc[2].trim(), splitteddesc[3].trim());
            descriptions.add(cdd);
        }
        return descriptions;
    }

}
