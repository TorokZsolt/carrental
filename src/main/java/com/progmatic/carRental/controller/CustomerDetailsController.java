/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.controller;

import com.progmatic.carRental.entities.Customer;
import com.progmatic.carRental.repositories.CountryRepository;
import com.progmatic.carRental.services.CountryService;
import com.progmatic.carRental.services.CustomerService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author NoDi
 */
@Controller
public class CustomerDetailsController {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    CustomerService customerservices;

    @Autowired
    CountryService countryservice;

    @RequestMapping(value = {"/customerDetails"}, method = RequestMethod.GET)
    public String showLogin(Model model) {
       model.addAttribute("pageTitle","Customer Details");
        model.addAttribute("customer", new Customer());
        model.addAttribute("countries", countryservice.findAll());
        return "customerDetails";
    }

    @RequestMapping(value = {"/customerList"}, method = RequestMethod.GET)
    public String showCustomer(
            @RequestParam(required = false, defaultValue = "10", name = "maxmsg") Integer msgMax,
            Model model) {
        model.addAttribute("customers", customerservices.findCustomer());
        model.addAttribute("pageTitle","Customer List");

        return "customerList";
    }

    @RequestMapping(value = {"/editCustomer/{id}"}, method = RequestMethod.GET)
    public String editCustomerById(
            @PathVariable("id") Integer customerId,
            Model model
    ) {
        Customer c = customerservices.findCustomerById(customerId);
        model.addAttribute("countries", countryservice.findAll());
        model.addAttribute("customer", c);
        model.addAttribute("pageTitle","Customer Editor");
        return "customerEditor";
    }

    @RequestMapping(path = "/customerDetails", method = RequestMethod.POST)
    public String createCustomer(@Valid Customer customer, BindingResult results,Model model){
        if(results.hasErrors()){
         model.addAttribute("countries", countryservice.findAll());   
            return "customerDetails";
        }
        customerservices.addCustomer(customer);
        return "redirect:/customerList";
    }

    @RequestMapping(value = {"/editCustomer/{id}"}, method = RequestMethod.POST)
    public String editCustomer(@Valid Customer customer,
            BindingResult results,
            @PathVariable("id") Integer customerId,
            Model model
    ) {
        if (results.hasErrors()) {
            model.addAttribute("countries", countryservice.findAll());
            return "customerEditor";
        }
        customer.setId(customerId);
        customerservices.save(customer);
        return "redirect:/customerList";
    }
}
