/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.controller;

import com.progmatic.carRental.entities.Invoice;
import com.progmatic.carRental.entities.Rent;
import com.progmatic.carRental.enums.RentStatusEnum;
import com.progmatic.carRental.services.InvoiceService;
import com.progmatic.carRental.services.RentService;
import com.progmatic.carRental.services.UserService;
import com.progmatic.carRental.xmlparts.InvoiceJAXB;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author User
 */
@Controller
public class InvoiceController {
    
    private static final Logger  LOG = LoggerFactory.getLogger(InvoiceController.class);

    @Autowired
    ApplicationContext context;

    @Autowired
    RentService rentservice;

    @Autowired
    InvoiceService invoiceservice;

    @Autowired
    InvoiceJAXB invoiceJaxb;
    
    @Autowired
    ServletContext servletContext;
    
    @Autowired
    UserService userService;

    @RequestMapping(value = {"/invoicing/{id}"}, method = RequestMethod.GET)
    public String showRentById(
            @PathVariable("id") Integer rentId,
            Model model) {

        Rent rent = rentservice.FindARentByID(rentId);
        if (rent.getInvoiceId() == null) {
            Invoice invoice = new Invoice(new Date(), new Date(), 0);
            invoiceservice.saveTheInvoice(invoice);
            rent.setInvoiceId(invoice);
            rentservice.save(rent);
        }
        model.addAttribute("rent", rent);
        return "invoice";
    }

    @RequestMapping(value = {"/createInvoice"}, method = RequestMethod.POST)
    public String saveTheInvoice (
            @RequestParam(name = "rentId") Integer rentId, Principal principal) throws Exception {
        //LOG.debug("saveTheInvoice rentId is: {}", rentId);
        // kikeressuk a rentet id alapjan
        Rent rent = rentservice.FindARentByID(rentId);
        Invoice invoice = rent.getInvoiceId();
        int days = rent.getRentingDay();
        int itemPrizeNett = (int)(rent.getCarCategoryId().getPrice()/1.27);
        int sumPrizeNett = days * itemPrizeNett;
        int gstSum = (int) (sumPrizeNett * 0.27);
        int sumPrizeGross = sumPrizeNett + gstSum;
        invoice.setAmount(sumPrizeGross);
        //Invoice invoice = new Invoice(new Date(), new Date(), sumPrizeGross);
        
        //XML MODEL előállítása
        File xml = invoiceJaxb.createXml(new Date(118,8,11), new Date(118,8,11), new Date(118,8,11),
            "Helyszíni fizetés.", rent.getId().toString(), rent.getFullName(),
            Integer.parseInt(rent.getCustomerId().getAddressZip()), rent.getCustomerId().getAddressCity(),
            rent.getCustomerId().getAddressStreet(), "12345678-1-43",
            rent.getCarId().getType(), days, itemPrizeNett, sumPrizeNett, gstSum,
            sumPrizeGross, "Kölcsönzési szolgáltatás díj.");
        //XML elküldése és a pdf kinyerése a responseból
        byte[] pdf = invoiceservice.sendWithHttps(xml, invoice);
        //PDF Lementése
        invoice.setInvoicePdf(pdf);
        //invoiceservice.saveTheInvoice(invoice);
        rent.setStatus(RentStatusEnum.FINISHED);
        // Aktuális user beállítása endUserId-nek
        rent.setEndUserId(userService.findByUserName(principal.getName()));
        rentservice.save(rent);
        return "redirect:/liveBookingsAndRentingsList";
    }
    
    @RequestMapping("/invoice/pdf/{pdfId}.pdf")
    @ResponseBody
    public void pdf(HttpServletResponse response, @PathVariable int pdfId) throws IOException {
        InputStream in = new ByteArrayInputStream(invoiceservice.getInvoicePdf(pdfId));
        response.setContentType(MediaType.APPLICATION_PDF_VALUE);
        IOUtils.copy(in, response.getOutputStream());
    }
    
    @RequestMapping(value = {"/invoiceList"}, method = RequestMethod.GET)
    public String showInvoiceList(Model model) {
        model.addAttribute("rentsList", rentservice.findFinishedRents());
        model.addAttribute("pageTitle", "Invoice List");
        return "invoiceList";
    }
}
