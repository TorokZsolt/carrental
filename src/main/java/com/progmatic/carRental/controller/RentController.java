/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.controller;

import com.progmatic.carRental.dto.RentAndCustomer;
import com.progmatic.carRental.entities.Customer;
import com.progmatic.carRental.entities.Rent;
import com.progmatic.carRental.entities.User;
import com.progmatic.carRental.services.CarCategoryService;
import com.progmatic.carRental.services.CountryService;
import com.progmatic.carRental.services.CustomerService;
import com.progmatic.carRental.services.RentService;
import com.progmatic.carRental.services.UserService;
import java.security.Principal;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author User
 */
@Controller
public class RentController {

    @Autowired
    ApplicationContext context;

    @Autowired
    RentService rentservice;
    
    @Autowired
    CustomerService customerservice;
    
    @Autowired
    CountryService countryservice;
    
    @Autowired
    CarCategoryService carCategoryService;
    

    @RequestMapping(value = {"/liveBookingsAndRentingsList"}, method = RequestMethod.GET)
    public String showBookings(Model model, HttpServletRequest request) {
        boolean showCancelledRentsToo = (Boolean) Optional.ofNullable(request.getSession()
                .getAttribute("showCancelledRentsToo")).orElse(false);
        boolean showFinishedRentsToo = (Boolean) Optional.ofNullable(request.getSession()
                .getAttribute("showFinishedRentsToo")).orElse(false);

        if (showCancelledRentsToo && showFinishedRentsToo) {
            model.addAttribute("rentsList", rentservice.findAllRentList());
            model.addAttribute("selectCancelled", true);
            model.addAttribute("selectFinished", true);

        } else if (!showCancelledRentsToo && showFinishedRentsToo) {
            model.addAttribute("rentsList", rentservice.findLiveAndFinishedRentsList());
            model.addAttribute("selectCancelled", false);
            model.addAttribute("selectFinished", true);

        } else if (showCancelledRentsToo && !showFinishedRentsToo) {
            model.addAttribute("rentsList", rentservice.findLiveAndCancelledRentsList());
            model.addAttribute("selectCancelled", true);
            model.addAttribute("selectFinished", false);

        } else {
            model.addAttribute("rentsList", rentservice.findLiveRentsList());
            model.addAttribute("selectCancelled", false);
            model.addAttribute("selectFinished", false);
        }
        model.addAttribute(
                "pageTitle", "Rent List");
        return "liveBookingsAndRentingsList";
    }

    @RequestMapping(value = {"/liveBookingsAndRentingsList/setCFVisible"}, method = RequestMethod.GET)
    public String rentListSetFinishedAndCancelledVisible(
            @RequestParam(required = false, name = "b", defaultValue = "0") Integer finished,
            @RequestParam(required = false, name = "a", defaultValue = "0") Integer cancelled,
            Model model, HttpServletRequest request) {
        request.getSession().setAttribute("showFinishedRentsToo", finished == 1);
        request.getSession().setAttribute("showCancelledRentsToo", cancelled == 1);
        return "redirect:/liveBookingsAndRentingsList";
    }
    
    @RequestMapping(value = {"/rentEditor/{id}"}, method = RequestMethod.GET)
    public String editRentById(
            @PathVariable("id") Integer rentId,
            Model model
    ) {
        Rent r = rentservice.FindARentByID(rentId);
        Customer customer = r.getCustomerId();
        
        RentAndCustomer rac = new  RentAndCustomer();
        rac.setRent(r);
        rac.setCustomer(customer);
        rac.setCarCatId(r.getCarId().getCarCategoryId().getId());
        
        
        //model.addAttribute("rent", rentservice.findAllRentList());
        model.addAttribute("rent", r);
        model.addAttribute("pageTitle","Rent Editor");
        model.addAttribute("rentAndCustomer", rac);
        model.addAttribute("countries", countryservice.findAll());
        model.addAttribute("carCategory", carCategoryService.findAll());
        return "rentEditor";
    }
    
    @RequestMapping(value = {"/rentEditor/{id}"},method = RequestMethod.POST)
    public String EditRent(@Valid RentAndCustomer rac, BindingResult results,
            @PathVariable("id") Integer customerId, Model model){
        if(results.hasErrors()){
            System.out.println("Valami Gebasz van!");
            model.addAttribute("countries", countryservice.findAll());
            model.addAttribute("carCategory", carCategoryService.findAll());
            return "rentEditor";
        }
        rac.getRent().setCustomerId(rac.getCustomer());
        rac.getCustomer().setId(customerId);
        rac.getRent().setCarCategoryId(carCategoryService.findByCategoryId(rac.getCarCatId()));
        try {
            customerservice.addCustomer(rac.getCustomer());
        } catch (DataIntegrityViolationException e) {
            rac.getRent().setCustomerId(customerservice.findCustomerByEmail(rac.getCustomer().getEmail()));
        }
        rentservice.save(rac.getRent());
        
        return "redirect:/liveBookingsAndRentingsList";
    }
    
    

}

