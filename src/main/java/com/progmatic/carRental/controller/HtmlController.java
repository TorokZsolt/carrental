/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author NoDi
 */
@Controller
public class HtmlController {

    @RequestMapping(value = {"/rentalconditions"}, method = RequestMethod.GET)
    public String showRentalConditions(Model model) {
        model.addAttribute("pageTitle", "Car Rental Conditions");
        return "rentalconditions";
    }

    @RequestMapping(value = {"/thankyou"}, method = RequestMethod.GET)
    public String showThankyou(Model model) {
        model.addAttribute("pageTitle", "Car Rental Conditions");
        return "thankyou";
    }       
}
