/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental;

import com.progmatic.carRental.entities.Booking;
import com.progmatic.carRental.entities.Car;
import com.progmatic.carRental.entities.CarCategory;
import com.progmatic.carRental.entities.Country;
import com.progmatic.carRental.entities.Customer;
import com.progmatic.carRental.entities.Role;
import com.progmatic.carRental.entities.Invoice;
import com.progmatic.carRental.entities.Rent;
import com.progmatic.carRental.entities.User;
import com.progmatic.carRental.enums.CarCategoryEnum;
import com.progmatic.carRental.enums.EngineTypeEnum;
import com.progmatic.carRental.enums.IdCardTypeEnum;
import com.progmatic.carRental.enums.RentStatusEnum;
import com.progmatic.carRental.enums.RoleEnum;
import com.progmatic.carRental.repositories.CarCategoryRepository;
import com.progmatic.carRental.repositories.CarRepository;
import com.progmatic.carRental.repositories.CountryRepository;
import com.progmatic.carRental.repositories.CustomerRepository;
import com.progmatic.carRental.repositories.InvoiceRepository;
import com.progmatic.carRental.repositories.RentRepository;
import com.progmatic.carRental.repositories.RoleRepository;
import com.progmatic.carRental.repositories.UserRepository;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import com.progmatic.carRental.repositories.BookingRepository;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;

/**
 *
 * @author User
 */
@Component
public class SpringDatabaseFiller {
    
    @Autowired
    CarRepository carRepository;
    
    @Autowired
    CarCategoryRepository carCategoryRepository;
    
    @Autowired
    RoleRepository roleRepository;
    
    @Autowired
    CustomerRepository customerRepository;
    
    @Autowired
    InvoiceRepository invoiceRepository;
    
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    RentRepository rentRepository;
    
    @Autowired
    CountryRepository countryRepository;
    
    @Autowired
    BookingRepository bookingRepository;
    
    @Autowired
    PasswordEncoder PASSWORD_ENCODER;
    
    @Autowired
    ServletContext servletContext;
    
    private List<Car> sampleCarList = new ArrayList<>();
    private List<CarCategory> sampleCarCategoryList = new ArrayList<>();
    private List<Role> sampleRoleList = new ArrayList<>();
    private List<Customer> sampleCustomerList = new ArrayList<>();
    private List<Invoice> sampleInvoiceList = new ArrayList<>();
    private List<User> sampleUserList = new ArrayList<>();
    private List<Rent> sampleRentList = new ArrayList<>();
    private List<Country> sampleCountryList = new ArrayList<>();
    private List<Booking> sampleBookingList = new ArrayList<>();
    
    @PostConstruct
    private void fillDb() throws IOException{
        fillCarCategories();
        fillCars();
        fillRoles();
        fillCountries();
        fillCustomers();
        //fillInvoices();
        fillUsers();
        fillRents();
        fillBookings();
    }
    
    
    // CarCategories
    
     private void fillCarCategories() throws IOException{
        if(carCategoryRepository.findAll().isEmpty()) {
        CarCategory categoryFDMR = new CarCategory(CarCategoryEnum.FDMR, 11500, "Moderline, 5 Doors, Automatic Shift, Dual Zone");
        sampleCarCategoryList.add(categoryFDMR);
        CarCategory categoryICMR = new CarCategory(CarCategoryEnum.ICMR, 7300, "Similar, 5 Doors, Manual Shift, Dual Zone");
        sampleCarCategoryList.add(categoryICMR);
        CarCategory categoryIFAR = new CarCategory(CarCategoryEnum.IFAR, 9500, "StreetsVille, 5 Doors, Automatic Shift, Dual Zone");
        sampleCarCategoryList.add(categoryIFAR);
        CarCategory categorySFMR = new CarCategory(CarCategoryEnum.SFMR, 9800, "Executive, 5 Doors, Automatic Shift, Dual Zone");
        sampleCarCategoryList.add(categorySFMR);
        CarCategory categoryEXMR = new CarCategory(CarCategoryEnum.EXMR, 5800, "Similar Sedan, 3 Doors, Manual Shift, Dual Zone");
        sampleCarCategoryList.add(categoryEXMR);
         CarCategory categoryCDMR = new CarCategory(CarCategoryEnum.CDMR, 6500, "Comportline, 5 Doors, Manual Shift, Dual Zone");
        sampleCarCategoryList.add(categoryCDMR);
         CarCategory categoryMCMN = new CarCategory(CarCategoryEnum.MCMN, 4700, "Move Up, 5 Doors, Manual Shift, Dual Zone");
        sampleCarCategoryList.add(categoryMCMN);
         CarCategory categoryXDAR = new CarCategory(CarCategoryEnum.XDAR, 59500, "Ghost, 5 Doors, Automatic Shift, Dual Zone");
        sampleCarCategoryList.add(categoryXDAR);
         CarCategory categoryEDMR = new CarCategory(CarCategoryEnum.EDMR, 5200, "PureTech, 5 Doors, Manual Shift, Dual Zone");
        sampleCarCategoryList.add(categoryEDMR);
         CarCategory categoryECMR = new CarCategory(CarCategoryEnum.ECMR, 5500, "Elefantino, 5 Doors, Manual Shift, Dual Zone");
        sampleCarCategoryList.add(categoryECMR);
         CarCategory categoryCLAE = new CarCategory(CarCategoryEnum.CLAE, 8100, "Station Wagon, 5 Doors, Manual Shift, Dual Zone");
        sampleCarCategoryList.add(categoryCLAE);
        CarCategory categoryPFAR = new CarCategory(CarCategoryEnum.PFAR, 16500, "Turbo TipTronic, 5 Doors, Automatic Shift, Dual Zone");
        sampleCarCategoryList.add(categoryPFAR);
        CarCategory categoryEDAR= new CarCategory(CarCategoryEnum.EDAR, 6700, "Comfort, 5 Doors, Manual Shift, Dual Zone");
        sampleCarCategoryList.add(categoryEDAR);
        CarCategory categoryCCCC = new CarCategory(CarCategoryEnum.CCCC, 6200, "Pulse SoftTouch, 5 Doors, Automatic Shift, Dual Zone");
        sampleCarCategoryList.add(categoryCCCC);
        carCategoryRepository.saveAll(sampleCarCategoryList);
        
        byte[] photo;
        photo = readImageToByteArray(categoryFDMR.getId());
        categoryFDMR.setPhoto(photo);
        
        photo = readImageToByteArray(categoryICMR.getId());
        categoryICMR.setPhoto(photo);
        
        photo = readImageToByteArray(categoryIFAR.getId());
        categoryIFAR.setPhoto(photo);
        
        photo = readImageToByteArray(categorySFMR.getId());
        categorySFMR.setPhoto(photo);
        
        photo = readImageToByteArray(categoryEXMR.getId());
        categoryEXMR.setPhoto(photo);
        
        photo = readImageToByteArray(categoryCDMR.getId());
        categoryCDMR.setPhoto(photo);
        
        photo = readImageToByteArray(categoryMCMN.getId());
        categoryMCMN.setPhoto(photo);
        
        photo = readImageToByteArray(categoryXDAR.getId());
        categoryXDAR.setPhoto(photo);
        
        photo = readImageToByteArray(categoryEDMR.getId());
        categoryEDMR.setPhoto(photo);
        
        photo = readImageToByteArray(categoryECMR.getId());
        categoryECMR.setPhoto(photo);
        
        photo = readImageToByteArray(categoryCLAE.getId());
        categoryCLAE.setPhoto(photo);
        
        photo = readImageToByteArray(categoryPFAR.getId());
        categoryPFAR.setPhoto(photo);
        
        photo = readImageToByteArray(categoryEDAR.getId());
        categoryEDAR.setPhoto(photo);
        
        photo = readImageToByteArray(categoryCCCC.getId());
        categoryCCCC.setPhoto(photo);
        
        carCategoryRepository.saveAll(sampleCarCategoryList);
        }
    }
    
    private byte[] readImageToByteArray(long imageId) throws IOException {
        InputStream in = servletContext.getResourceAsStream(imageId + ".jpg");                
        return StreamUtils.copyToByteArray(in);
    }

    // Cars
    
    private void fillCars(){
        if(carRepository.findAll().isEmpty()){
        Car car1 = new Car("RTK-234", "5UXFB33533LH40739", "521WCG0456", "fekete", "BMW 3-Series", EngineTypeEnum.PETROL, true);
        setCategoryAndCarToList(car1, "FDMR");
        Car car2 = new Car("LFK-432", "6KUZP44567PK43213", "767KZT8743", "szürke", "Subaru Impreza", EngineTypeEnum.DIESEL, true);
        setCategoryAndCarToList(car2, "ICMR");
        Car car3 = new Car("UZH-897", "TZU6768567ZT78912", "675KZU2536", "szürke", "Hunday Santafe Xl", EngineTypeEnum.DIESEL, true);
        setCategoryAndCarToList(car3, "IFAR");
        Car car4 = new Car("ULH-847", "KZU6768567ZT78934", "625KZU2539", "szürke", "Honda Vezel", EngineTypeEnum.GAS, true);
        setCategoryAndCarToList(car4, "SFMR");
        Car car5 = new Car("ZTF-823", "LZP6768567ZT78975", "875KZU2537", "piros", "Renault Clio", EngineTypeEnum.DIESEL, true);
        setCategoryAndCarToList(car5, "EXMR");
        Car car6 = new Car("KLI-567", "SDR6768567ZT78914", "685KZU2534", "fehér", "Volkswagen Golf", EngineTypeEnum.PETROL, true);
        setCategoryAndCarToList(car6, "CDMR");
        Car car7 = new Car("VFZ-587", "SDR6768567ZT78916", "667KZU2534", "piros", "Volkswagen Up", EngineTypeEnum.PETROL, true);
        setCategoryAndCarToList(car7, "MCMN");
        Car car8 = new Car("RZE-579", "SDR6768567ZT78911", "689KZL2534", "fehér", "Rolls Royce", EngineTypeEnum.PETROL, true);
        setCategoryAndCarToList(car8, "XDAR");
        Car car9 = new Car("NFT-285", "SDR6768547ZT78978", "685FZE2534", "fehér", "Citroen C3", EngineTypeEnum.DIESEL, true);
        setCategoryAndCarToList(car9, "EDMR");
        Car car10 = new Car("LOF-547", "SDR6768567ZT78818", "685KZU3434", "fehér", "Lancia Ypsilon", EngineTypeEnum.PETROL, false);
        setCategoryAndCarToList(car10, "ECMR");
        Car car11 = new Car("DTU-994", "SDR6768567ZT78118", "985KZU2534", "fehér", "Toyota Innova", EngineTypeEnum.PETROL, true);
        setCategoryAndCarToList(car11, "CLAE");
        Car car12 = new Car("DRZ-384", "SDR6768567ZJ77918", "685KUU2534", "fehér", "Porsche Cayenne", EngineTypeEnum.DIESEL, false);
        setCategoryAndCarToList(car12, "PFAR");
        Car car13= new Car("ZOP-164", "SDR6768567ZT78978", "813KZU2534", "fehér", "Hunday i10", EngineTypeEnum.PETROL, true);
        setCategoryAndCarToList(car13, "EDAR");
        Car car14 = new Car("EWT-856", "SDR6768567ZT78919", "936KOT2534", "fekete", "Smart ForFour", EngineTypeEnum.DIESEL, false);
        setCategoryAndCarToList(car14, "CCCC");
        carRepository.saveAll(sampleCarList);
        }
    }
    
    private void setCategoryAndCarToList(Car car, String category){
        car.setCarCategoryId(carCategoryRepository.findByName(category));                                                       //kikeresi az "A" kategóriát az adatbázisból és azt köti össze az autóval
        sampleCarList.add(car);
    }
    
    
    // Roles
    
    private void fillRoles() {
        if(roleRepository.findAll().isEmpty()){
        Role role1 = new Role(RoleEnum.ROLE_ADMIN);
        sampleRoleList.add(role1);
        Role role2 = new Role(RoleEnum.ROLE_USER);
        sampleRoleList.add(role2);
        roleRepository.saveAll(sampleRoleList);
        }
    }
    
    
    // Customers
    
    private void fillCustomers() {
        if(customerRepository.findAll().isEmpty()){
        Date date1 = new Date(119,9,30);
        Date date2 = new Date(118,12,31);
        Customer customer1 = new Customer("Béla", "Tóth", "1234", countryRepository.findByCountryCode("HUN"), "Budapest", "Minta u. 1.", 
                "CZ1234567", IdCardTypeEnum.IDCARD, "579023KA", date1, "bela.toth@email.com", "06301234567");
        sampleCustomerList.add(customer1);
        Customer customer2 = new Customer("Gyula", "Kiss", "1143", countryRepository.findByCountryCode("US"), "Budapest", "Teszt tér. 1.", 
                "KL7654321", IdCardTypeEnum.IDCARD, "899023LJ", date2, "gyula.kiss@email.com", "06707654321");
        sampleCustomerList.add(customer2);
        customerRepository.saveAll(sampleCustomerList);
        }
    }
    
    
    // Invoices
    
    private void fillInvoices() {
        if(invoiceRepository.findAll().isEmpty()){ 
        Date date1 = new Date(118,8,11);
        Date date2 = new Date(118,8,11);
        Date date3 = new Date(118,8,11);
        Date date4 = new Date(118,8,11);
        Invoice invoice1 = new Invoice("", date1, date2, 58000);
        sampleInvoiceList.add(invoice1);
        Invoice invoice2 = new Invoice("", date3, date4, 40000);
        sampleInvoiceList.add(invoice2);
        Invoice invoice3 = new Invoice("", date3, date4, 5000);
        sampleInvoiceList.add(invoice3);
        invoiceRepository.saveAll(sampleInvoiceList);
        }
    }
    
    
    // Users

    private void fillUsers() {
        if(userRepository.findAll().isEmpty()){
        User user1 = new User("Kovács Zsuzsa", "KZsuzsa", PASSWORD_ENCODER.encode("zsuzsa"), true);
        userToList(user1, 1);
        User user2 = new User("Varga Mihály", "VMisi", PASSWORD_ENCODER.encode("misi"), true);
        userToList(user2, 2);
        User user3 = new User("Kiss Gyula", "KGyula", PASSWORD_ENCODER.encode("gyula"), false);
        userToList(user3, 2);
        userRepository.saveAll(sampleUserList);
        }
    }

    private void userToList(User user, int numb){
        user.setRoleId(roleRepository.findById(numb));
        sampleUserList.add(user);
    } 
    
    
    private static Date dateOf(int year, int month, int day){
        LocalDate ld = LocalDate.of(year, Month.of(month), day);
        return Date.from(ld.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
    
    
    // Rents
    
    private void fillRents() {
        if(rentRepository.findAll().isEmpty()){
        Date date1 = dateOf(2018, 8, 1);
        Date date2 = dateOf(2018, 8, 3);
        Date date3 = dateOf(2018, 8, 6);
        Date date4 = dateOf(2018, 8, 7);
        Date date5 = dateOf(2018, 8, 12);
        Date date6 = dateOf(2018, 8, 13);
        Date date7 = dateOf(2018, 8, 12);
        Date date8 = dateOf(2018, 8, 13);
        Date date9 = dateOf(2018, 1, 11);
        Date date10 = dateOf(2018, 1, 28);
        Date date11 = dateOf(2018, 2, 1);
        Date date12 = dateOf(2018, 1, 21);
        Date date13 = dateOf(2018, 2, 8);
        Date date14 = dateOf(2018, 2, 27);
        Date date15 = dateOf(2018, 3, 8);
        Date date16 = dateOf(2018, 3, 30);
        Date date17 = dateOf(2018, 4, 3);
        Date date18 = dateOf(2018, 4, 19);
        Date date19 = dateOf(2018, 5, 5);
        Date date20 = dateOf(2018, 5, 20);
        Date date21 = dateOf(2018, 6, 8);
        Date date22 = dateOf(2018, 6, 29);
        Date date23 = dateOf(2018, 7, 13);
        Date date24 = dateOf(2018, 7, 23);
        Date date25 = dateOf(2018, 9, 4);
        Date date26 = dateOf(2018, 9, 27);
        Date date27 = dateOf(2018, 10, 1);
        Date date28 = dateOf(2018, 10, 18);
        Date date29 = dateOf(2018, 11, 11);
        Date date30 = dateOf(2018, 11, 20);
        Date date31 = dateOf(2018, 12, 6);
        Date date32 = dateOf(2018, 12, 31);
        Date date33 = dateOf(2019, 1, 1);
        Date date34 = dateOf(2019, 1, 11);
        Date date35 = dateOf(2019, 2, 2);
        Date date36 = dateOf(2019, 2, 25);
        Date date37 = dateOf(2019, 3, 9);
        Date date38 = dateOf(2019, 3, 15);
        Date date39 = dateOf(2019, 4, 1);
        Date date40 = dateOf(2019, 4, 20);
        Date date41 = dateOf(2019, 5, 8);
        Date date42 = dateOf(2019, 5, 18);
        Date date43 = dateOf(2019, 6, 6);
        Date date44 = dateOf(2019, 6, 19);
        Date date45 = dateOf(2019, 7, 2);
        Date date46 = dateOf(2019, 7, 24);
        Date date47 = dateOf(2019, 8, 6);
        Date date48 = dateOf(2019, 8, 29);
        Date date49 = dateOf(2019, 9, 4);
        Date date50 = dateOf(2019, 9, 27);
        Date date51 = dateOf(2019, 10, 1);
        Date date52 = dateOf(2019, 10, 31);
        Date date53 = dateOf(2019, 11, 1);
        Date date54 = dateOf(2019, 11, 11);
        Date date55 = dateOf(2019, 12, 3);
        Date date56 = dateOf(2019, 12, 18);
        
        Rent rent1 = new Rent(date1, date2, RentStatusEnum.RENT, 100, 250, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 60000, 0, "hash123456789");
        rentToList(rent1, 2, 2, 1, 1);
        
        Rent rent2 = new Rent(date3, date4, RentStatusEnum.CANCELLED, 100, 0, (short)100, (short)0, 
                "Kölcsönző", "Kölcsönző", 60000, 0, "hash987654321");
        rentToList(rent2, 2, 2, 1, 2, 2);
        
        Rent rent3 = new Rent(date1, date2, RentStatusEnum.RENT, 100, 225, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 58000, 0, "hash223456787");
        rentToList(rent3, 2, 2, 2, 1);
        
        Rent rent4 = new Rent(date3, date4, RentStatusEnum.CANCELLED, 100, 0, (short)100, (short)0, 
                "Kölcsönző", "Kölcsönző", 58000, 0, "hash687654324");
        rentToList(rent4, 2, 2, 1, 2, 2);
        
        Rent rent5 = new Rent(date5, date6, RentStatusEnum.FINISHED, 100, 215, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 40000, 40000, "hash623456784");
        rentToList(rent5, 3, 3, 1, 1, 1);
        
        Rent rent6 = new Rent(date5, date6, RentStatusEnum.RENT, 100, 0, (short)100, (short)0, 
                "Kölcsönző", "Kölcsönző", 40000, 0, "hash287654329");
        rentToList(rent6, 3, 3, 1, 2);
        
        Rent rent7 = new Rent(date7, date8, RentStatusEnum.FINISHED, 100, 250, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 60000, 60000, "hash323456786");
        rentToList(rent7, 4, 4, 2, 1, 1);
        
        Rent rent8 = new Rent(date7, date8, RentStatusEnum.RENT, 100, 0, (short)100, (short)0, 
                "Kölcsönző", "Kölcsönző", 60000, 0, "hash187654327");
        rentToList(rent8, 4, 4, 2, 2);
        
        Rent rent9 = new Rent(date1, date2, RentStatusEnum.FINISHED, 100, 250, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 35000, 35000, "hash323456789");
        rentToList(rent9, 4, 4, 1, 1, 1);
        
        Rent rent10 = new Rent(date3, date4, RentStatusEnum.FINISHED, 100, 215, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 60000, 60000, "hash187654321");
        rentToList(rent10, 5, 5, 1, 2, 2);
        
        Rent rent11 = new Rent(date5, date6, RentStatusEnum.FINISHED, 100, 225, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 30000, 0, "hash523456789");
        rentToList(rent11, 6, 6, 2, 1, 1);
        
        Rent rent12 = new Rent(date7, date8, RentStatusEnum.FINISHED, 100, 195, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 60000, 60000, "hash187654325");
        rentToList(rent12, 4, 4, 1, 2, 2);
        
        Rent rent13 = new Rent(date5, date6, RentStatusEnum.FINISHED, 100, 215, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 40000, 40000, "hash223456783");
        rentToList(rent13, 3, 3, 1, 1, 1);
        
        Rent rent14 = new Rent(date5, date6, RentStatusEnum.FINISHED, 100, 210, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 40000, 0, "hash687654329");
        rentToList(rent14, 3, 3, 1, 2, 2);
        
        Rent rent15 = new Rent(date7, date8, RentStatusEnum.FINISHED, 100, 230, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 60000, 60000, "hash523456785");
        rentToList(rent15, 2, 2, 2, 1, 1);
        
        Rent rent16 = new Rent(date7, date8, RentStatusEnum.FINISHED, 100, 215, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 58000, 58000, "hash287654324");
        rentToList(rent16, 2, 2, 2, 2, 2);
        
        Rent rent17 = new Rent(date9, date10, RentStatusEnum.RENT, 0, 125, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 58000, 58000, "hash287654325");
        rentToList(rent17, 3, 3, 2, 2);
        
        Rent rent18 = new Rent(date8, date9, RentStatusEnum.FINISHED, 25, 115, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 58000, 58000, "hash287654326");
        rentToList(rent18, 4, 4, 2, 2, 2);
        
        Rent rent19 = new Rent(date10, date11, RentStatusEnum.RENT, 100, 215, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 27000, 0, "hash287654327");
        rentToList(rent19, 5, 5, 2, 2);
        
        Rent rent20 = new Rent(date11, date12, RentStatusEnum.FINISHED, 100, 215, (short)100, (short)100, 
                "Ferihegy", "Kölcsönző", 44000, 44000, "hash287654328");
        rentToList(rent20, 6, 6, 2, 2, 2);
        
        Rent rent201 = new Rent(date11, date12, RentStatusEnum.FINISHED, 100, 215, (short)100, (short)100, 
                "Ferihegy", "Kölcsönző", 44000, 44000, "hash287154328");
        rentToList(rent201, 1, 1, 2, 2, 2);
        
        Rent rent202 = new Rent(date11, date12, RentStatusEnum.FINISHED, 100, 215, (short)100, (short)100, 
                "Ferihegy", "Kölcsönző", 44000, 44000, "hash287254328");
        rentToList(rent202, 2, 2, 2, 2, 2);
        
        Rent rent21 = new Rent(date12, date13, RentStatusEnum.RENT, 125, 235, (short)100, (short)100, 
                "Kölcsönző", "Ferihegy", 36000, 36000, "hash287654529");
        rentToList(rent21, 7, 7, 2, 2);
        
        Rent rent22 = new Rent(date13, date14, RentStatusEnum.RENT, 200, 335, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 36000, 36000, "hash287654330");
        rentToList(rent22, 8, 8, 2, 2);
        
        Rent rent23 = new Rent(date14, date15, RentStatusEnum.FINISHED, 285, 375, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 11000, 11000, "hash287654331");
        rentToList(rent23, 9, 9, 2, 2, 2);
        
        Rent rent24 = new Rent(date15, date16, RentStatusEnum.RENT, 322, 488, (short)100, (short)100, 
                "Kölcsönző", "Ferihegy", 45000, 45000, "hash287654332");
        rentToList(rent24, 10, 10, 2, 2);
        
        Rent rent25 = new Rent(date16, date17, RentStatusEnum.FINISHED, 338, 500, (short)100, (short)100, 
                "Ferihegy", "Kölcsönző", 42000, 42000, "hash287654333");
        rentToList(rent25, 11, 11, 2, 2, 2);
        
        Rent rent26 = new Rent(date17, date18, RentStatusEnum.RENT, 338, 500, (short)100, (short)100, 
                "Ferihegy", "Kölcsönző", 22000, 22000, "hash287654334");
        rentToList(rent26, 12, 12, 2, 2);
        
        Rent rent27 = new Rent(date17, date18, RentStatusEnum.RENT, 412, 510, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 28000, 28000, "hash287654335");
        rentToList(rent27, 13, 13, 2, 2);
        
        Rent rent28 = new Rent(date18, date19, RentStatusEnum.FINISHED, 485, 625, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 56000, 56000, "hash287654336");
        rentToList(rent28, 14, 14, 2, 2, 2);
        
        Rent rent29 = new Rent(date19, date20, RentStatusEnum.RENT, 282, 425, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 44000, 44000, "hash287654337");
        rentToList(rent29, 1, 1, 2, 2);
                
        Rent rent219 = new Rent(date19, date20, RentStatusEnum.RENT, 282, 425, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 44000, 44000, "hash2876543371");
        rentToList(rent219, 2, 2, 2, 2);
        
        Rent rent229 = new Rent(date19, date20, RentStatusEnum.RENT, 282, 425, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 44000, 44000, "hash2876543372");
        rentToList(rent229, 3, 3, 2, 2);
        
        Rent rent239 = new Rent(date19, date20, RentStatusEnum.RENT, 282, 425, (short) 100, (short) 100,
                 "Kölcsönző", "Kölcsönző", 44000, 44000, "hash2876543373");
        rentToList(rent239, 4, 4, 2, 2);
        
        Rent rent249 = new Rent(date19, date20, RentStatusEnum.RENT, 282, 425, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 44000, 44000, "hash2876543374");
        rentToList(rent249, 5, 5, 2, 2);
        
        Rent rent259 = new Rent(date19, date20, RentStatusEnum.RENT, 282, 425, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 44000, 44000, "hash2876543375");
        rentToList(rent259, 6, 6, 2, 2);
        
        Rent rent269 = new Rent(date19, date20, RentStatusEnum.RENT, 282, 425, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 44000, 44000, "hash2876543376");
        rentToList(rent269, 7, 7, 2, 2);
        
         Rent rent279 = new Rent(date19, date20, RentStatusEnum.RENT, 282, 425, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 44000, 44000, "hash2876543377");
        rentToList(rent279, 7, 7, 2, 2);
        
        Rent rent30 = new Rent(date20, date21, RentStatusEnum.RENT, 292, 487, (short)100, (short)100, 
                "Ferihegy", "Kölcsönző", 50000, 50000, "hash287654338");
        rentToList(rent30, 2, 2, 2, 2);
        
        Rent rent31 = new Rent(date21, date22, RentStatusEnum.FINISHED, 500, 650, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 37000,37000, "hash287654339");
        rentToList(rent31, 3, 3, 2, 2, 2);
        
        Rent rent32 = new Rent(date22, date23, RentStatusEnum.RENT, 438, 585, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 42000, 42000, "hash287654340");
        rentToList(rent32, 4, 4, 2, 2);
        
        Rent rent33 = new Rent(date23, date24, RentStatusEnum.RENT, 500, 600, (short)100, (short)100, 
                "Kölcsönző", "Ferihegy", 38000, 38000, "hash287654341");
        rentToList(rent33, 5, 5, 2, 2);
        
        Rent rent34 = new Rent(date24, date25, RentStatusEnum.FINISHED, 550, 640, (short)100, (short)100, 
                "Kölcsönző", "Ferihegy", 41000, 41000, "hash287654342");
        rentToList(rent34, 6, 6, 2, 2, 2);
        
        Rent rent35 = new Rent(date25, date26, RentStatusEnum.RENT, 580, 700, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 48000, 48000, "hash287654343");
        rentToList(rent35, 7, 7, 2, 2);
        
        Rent rent36 = new Rent(date26, date27, RentStatusEnum.RENT, 650, 785, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 43000, 43000, "hash287654344");
        rentToList(rent36, 8, 8, 2, 2);
        
        Rent rent37 = new Rent(date27, date28, RentStatusEnum.FINISHED, 850, 985, (short)100, (short)100, 
                "Kölcsönző", "Ferihegy", 49000, 49000, "hash287654345");
        rentToList(rent37, 9, 9, 2, 2, 2);
        
        Rent rent38 = new Rent(date28, date29, RentStatusEnum.RENT, 580, 795, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 55000, 55000, "hash287654346");
        rentToList(rent38, 10, 10, 2, 2);
        
        Rent rent39 = new Rent(date29, date30, RentStatusEnum.FINISHED, 1000, 1122, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 56000, 56000, "hash287654347");
        rentToList(rent39, 11, 11, 2, 2, 2);
        
        Rent rent40 = new Rent(date30, date31, RentStatusEnum.RENT, 850, 980, (short)100, (short)100, 
                "Ferihegy", "Kölcsönző", 54000, 54000, "hash287654348");
        rentToList(rent40, 12, 12, 2, 2);
        
        Rent rent41 = new Rent(date31, date32, RentStatusEnum.RENT, 800, 1000, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 41000, 41000, "hash287654349");
        rentToList(rent41, 13, 13, 2, 2);
        
        Rent rent411 = new Rent(date31, date32, RentStatusEnum.RENT, 800, 1000, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 41000, 41000, "hash2876543491");
        rentToList(rent411, 13, 13, 2, 2);
        
        Rent rent412 = new Rent(date31, date32, RentStatusEnum.RENT, 800, 1000, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 41000, 41000, "hash2876543492");
        rentToList(rent412, 12, 12, 2, 2);
        
        Rent rent413 = new Rent(date31, date32, RentStatusEnum.RENT, 800, 1000, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 41000, 41000, "hash2876543493");
        rentToList(rent413, 11, 11, 2, 2);
        
        Rent rent414 = new Rent(date31, date32, RentStatusEnum.RENT, 800, 1000, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 41000, 41000, "hash2876543494");
        rentToList(rent414, 10, 10, 2, 2);
        
        Rent rent415 = new Rent(date31, date32, RentStatusEnum.RENT, 800, 1000, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 41000, 41000, "hash2876543495");
        rentToList(rent415, 9, 9, 2, 2);
        
        Rent rent416 = new Rent(date31, date32, RentStatusEnum.RENT, 800, 1000, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 41000, 41000, "hash2876543496");
        rentToList(rent416, 11, 11, 2, 2);
        
         Rent rent42 = new Rent(date32, date33, RentStatusEnum.FINISHED, 1010, 1110, (short)100, (short)100, 
                "Kölcsönző", "Ferihegy", 41000, 41000, "hash287654350");
        rentToList(rent42, 14, 14, 2, 2, 2);
        
         Rent rent43 = new Rent(date33, date34, RentStatusEnum.RENT, 880, 1300, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 74000, 74000, "hash287654351");
        rentToList(rent43, 1, 1, 2, 2);
        
         Rent rent44 = new Rent(date34, date35, RentStatusEnum.RENT, 1000, 1050, (short)100, (short)100, 
                "Ferihegy", "Kölcsönző", 41000, 41000, "hash287654352");
        rentToList(rent44, 2, 2, 2, 2);
        
         Rent rent45 = new Rent(date31, date32, RentStatusEnum.FINISHED, 1020, 1240, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 51000, 51000, "hash287654353");
        rentToList(rent45, 3, 3, 2, 2, 2);
        
        Rent rent46 = new Rent(date32, date33, RentStatusEnum.RENT, 1080, 1240, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 46000, 46000, "hash287654354");
        rentToList(rent46, 4, 4, 2, 2);
        
        Rent rent47 = new Rent(date33, date34, RentStatusEnum.RENT, 1050, 1270, (short)100, (short)100, 
                "Kölcsönző", "Ferihegy", 55000, 55000, "hash287654355");
        rentToList(rent47, 5, 5, 2, 2);
        
        Rent rent48 = new Rent(date34, date35, RentStatusEnum.FINISHED, 1220, 1440, (short)100, (short)100, 
                "Kölcsönző", "Ferihegy", 51000, 51000, "hash287654356");
        rentToList(rent48, 6, 6, 2, 2, 2);
        
        Rent rent49 = new Rent(date35, date36, RentStatusEnum.RENT, 1120, 1340, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 39000, 39000, "hash287654357");
        rentToList(rent49, 7, 7, 2, 2);
        
        Rent rent50 = new Rent(date36, date37, RentStatusEnum.RENT, 1100, 1280, (short)100, (short)100, 
                "Ferihegy", "Kölcsönző", 40000, 40000, "hash287657358");
        rentToList(rent50, 8, 8, 2, 2);
        
        Rent rent51 = new Rent(date37, date38, RentStatusEnum.FINISHED, 1380, 1510, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 40000, 40000, "hash287654358");
        rentToList(rent51, 8, 8, 2, 2, 2);
        
        Rent rent52 = new Rent(date38, date39, RentStatusEnum.RENT, 1090, 1380, (short)100, (short)100, 
                "Ferihegy", "Kölcsönző", 43000, 43000, "hash287654359");
        rentToList(rent52, 9, 9, 2, 2);
        
        Rent rent53 = new Rent(date39, date40, RentStatusEnum.RENT, 1260, 1480, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 44000, 44000, "hash287654360");
        rentToList(rent53, 10, 10, 2, 2);
        
        Rent rent54 = new Rent(date40, date41, RentStatusEnum.FINISHED, 1480, 1680, (short)100, (short)100, 
                "Kölcsönző", "Ferihegy", 46000, 46000, "hash287657361");
        rentToList(rent54, 11, 11, 2, 2, 2);
        
        Rent rent55 = new Rent(date41, date42, RentStatusEnum.RENT, 1360, 1540, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 47000, 47000, "hash287654362");
        rentToList(rent55, 12, 12, 2, 2);
        
        Rent rent56 = new Rent(date43, date44, RentStatusEnum.RENT, 1420, 1690, (short)100, (short)100, 
                "Kölcsönző", "Ferihegy", 51000, 51000, "hash287654361");
        rentToList(rent56, 13, 13, 2, 2);
        
        Rent rent57 = new Rent(date44, date45, RentStatusEnum.FINISHED, 1640, 1880, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 56000, 56000, "hash287654762");
        rentToList(rent57, 14, 14, 2, 2, 2);
        
        Rent rent58 = new Rent(date45, date46, RentStatusEnum.RENT, 1510, 1780, (short)100, (short)100, 
                "Kölcsönző", "Ferihegy", 49000, 49000, "hash287654363");
        rentToList(rent58, 1, 1, 2, 2);
        
        Rent rent59 = new Rent(date46, date47, RentStatusEnum.RENT, 1620, 1820, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 50000, 50000, "hash287654364");
        rentToList(rent59, 2, 2, 2, 2);
        
        Rent rent60 = new Rent(date47, date48, RentStatusEnum.FINISHED, 1810, 2010, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 51000, 51000, "hash287654365");
        rentToList(rent60, 3, 3, 2, 2, 2);
        
        Rent rent61 = new Rent(date48, date49, RentStatusEnum.RENT, 1800, 2080, (short)100, (short)100, 
                "Ferihegy", "Kölcsönző", 52000, 52000, "hash287654366");
        rentToList(rent61, 4, 4, 2, 2);
        
        Rent rent62 = new Rent(date49, date50, RentStatusEnum.RENT, 1810, 2110, (short)100, (short)100, 
                "Kölcsönző", "Ferihegy", 52000, 52000, "hash287654367");
        rentToList(rent62, 5, 5, 2, 2);
        
        Rent rent63 = new Rent(date50, date51, RentStatusEnum.FINISHED, 2020, 2310, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 64000, 64000, "hash287654368");
        rentToList(rent63, 6, 6, 2, 2, 2);
        
        Rent rent64 = new Rent(date51, date52, RentStatusEnum.RENT, 2110, 2210, (short)100, (short)100, 
                "Ferihegy", "Kölcsönző", 62000, 62000, "hash287654369");
        rentToList(rent64, 7, 7, 2, 2);
        
        Rent rent65 = new Rent(date52, date53, RentStatusEnum.RENT, 1810, 2010, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 54000, 54000, "hash287654370");
        rentToList(rent65, 8, 8, 2, 2);
        
        Rent rent66 = new Rent(date53, date54, RentStatusEnum.FINISHED, 2380, 2500, (short)100, (short)100, 
                "Kölcsönző", "Ferihegy", 56000, 56000, "hash287654371");
        rentToList(rent66, 9, 9, 2, 2, 2);
        
        Rent rent67 = new Rent(date54, date55, RentStatusEnum.RENT, 2220, 2460, (short)100, (short)100, 
                "Ferihegy", "Ferihegy", 54000, 54000, "hash287654372");
        rentToList(rent67, 9, 9, 2, 2);
        
        Rent rent68 = new Rent(date55, date56, RentStatusEnum.RENT, 1810, 2010, (short)100, (short)100, 
                "Kölcsönző", "Kölcsönző", 46000, 46000, "hash287654373");
        rentToList(rent68, 10, 10, 2, 2);
        
        
        
        rentRepository.saveAll(sampleRentList);
        }
    }

    private void rentToList(Rent rent, int n1, int n2, int n3, int n4//, int n5, int n6, int n7
            ){
        rent.setCarCategoryId(carCategoryRepository.findById(n1));
        rent.setCarId(carRepository.findById(n2));
        rent.setCustomerId(customerRepository.findById(n3));
        rent.setStartUserId(userRepository.findById(n4));
        //rent.setEndUserId(userRepository.findById(n5));
        //rent.setInvoiceId(invoiceRepository.findById(n6));
        //rent.setExtraInvoiceId(invoiceRepository.findById(n7));
        sampleRentList.add(rent);
    } 
    
    private void rentToList(Rent rent, int n1, int n2, int n3, int n4, int n5//, int n6, int n7
            ){
        rent.setCarCategoryId(carCategoryRepository.findById(n1));
        rent.setCarId(carRepository.findById(n2));
        rent.setCustomerId(customerRepository.findById(n3));
        rent.setStartUserId(userRepository.findById(n4));
        rent.setEndUserId(userRepository.findById(n5));
        //rent.setInvoiceId(invoiceRepository.findById(n6));
        //rent.setExtraInvoiceId(invoiceRepository.findById(n7));
        sampleRentList.add(rent);
    }
    
//    private void rentToList(Rent rent, int n1, int n2, int n3, int n4, int n5 , int n6, int n7){
//        rent.setCarCategoryId(carCategoryRepository.findById(n1));
//        rent.setCarId(carRepository.findById(n2));
//        rent.setCustomerId(customerRepository.findById(n3));
//        rent.setStartUserId(userRepository.findById(n4));
//        rent.setEndUserId(userRepository.findById(n5));
//        rent.setInvoiceId(invoiceRepository.findById(n6));
//        rent.setExtraInvoiceId(invoiceRepository.findById(n7));
//        sampleRentList.add(rent);
//    } 

    // Countries

    private void fillCountries() {
        if(rentRepository.findAll().isEmpty()){
        Country country1 = new Country("HUN", "Magyarország"); 
        sampleCountryList.add(country1);
        Country country2 = new Country("AT", "Ausztria"); 
        sampleCountryList.add(country2);
        Country country3 = new Country("US", "Amerikai Egyesült Államok"); 
        sampleCountryList.add(country3);
        Country country4 = new Country("HR", "Horvátország"); 
        sampleCountryList.add(country4);
        Country country5 = new Country("ES", "Spanyolország"); 
        sampleCountryList.add(country5);
        countryRepository.saveAll(sampleCountryList);
        }
        
    }

    public List<Country> getSampleCountryList() {
        return sampleCountryList;
    }
    
    // Books

    private void fillBookings() {
        if (bookingRepository.findAll().isEmpty()) {
            Date date1 = new Date(118,7,31);
            Date date2 = new Date(118,8,3);
            Date date3 = new Date(118,8,6);
            Date date4 = new Date(118,8,7);
            Booking booking1 = new Booking("Zoltán", "Tóth", "zoltan.toth@email.com", "06301234567", "Kölcsönző", "Kölcsönző", date1, date2, "hash72345671");
            booking1.setCarId(carRepository.findById(1));
            sampleBookingList.add(booking1);
            Booking booking2 = new Booking("Béla", "Kovács", "bela.kovacs@email.com", "06306234568", "Reptér", "Kölcsönző", date3, date4, "hash62345672");
            booking2.setCarId(carRepository.findById(2));
            sampleBookingList.add(booking2);
            Booking booking3 = new Booking("Csilla", "Szabó", "csilla.szabo@email.com", "06303234569", "Kölcsönző", "Reptér", date1, date2, "hash52345679");
            booking3.setCarId(carRepository.findById(3));
            sampleBookingList.add(booking3);
            bookingRepository.saveAll(sampleBookingList);
        }
    }
    
}
   
