/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.dto;

/**
 *
 * @author benko
 */
public class AvailableCarsDto {
    
    private int id;
    private String licencePlate;
    private String type;

    public AvailableCarsDto(int id, String licencePlate, String type) {
        this.id = id;
        this.licencePlate = licencePlate;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
    
}
