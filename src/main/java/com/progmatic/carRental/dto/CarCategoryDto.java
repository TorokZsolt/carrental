/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.dto;

/**
 *
 * @author Molnár Richárd
 */
public class CarCategoryDto {
    private Integer id;
    private String name;
    private int price;
    private String description;
    private byte[] photo;
    private long rentCountPerMonth;

    public CarCategoryDto(Integer id, String name, int price, String description, byte[] photo) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.photo = photo;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public long getRentCountPerMonth() {
        return rentCountPerMonth;
    }

    public void setRentCountPerMonth(long rentCountPerMonth) {
        this.rentCountPerMonth = rentCountPerMonth;
    }
    
   
}
