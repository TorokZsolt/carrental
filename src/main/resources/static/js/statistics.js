/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function () {

    var months = [
        'January', 'February', 'March', 'April', 'May',
        'June', 'July', 'August', 'September',
        'October', 'November', 'December'
    ];

    function monthNumToName(monthnum) {
        return months[monthnum - 1] || '';
    }
    
    $("#year").change(function () {
        var chart_id = window.location.pathname.split("/chart/")[1];
        $.get("/getmonths", {year: $('#year').val(), chartId: chart_id})
                .done(function (data) {
                    $("#month").empty();
                    if (chart_id === "finished_car") {
                        $.get("/getfinishedstatistics", {year: $('#year').val(), month: data[0]})
                            .done(function (data) {
                                drawChart(data);
                            });
                    } else if (chart_id === "booked_car") {
                        $.get("/getbookedstatistics", {year: $('#year').val(), month: data[0]})
                            .done(function (data) {
                                drawChartPie(data);
                            });
                    } else if (chart_id === "rent_car") {
                        $.get("/getonrentstatistics", {year: $('#year').val(), month: data[0]})
                            .done(function (data) {
                                drawChart3d(data);
                            });
                    }
                    for (var i = 0; i < data.length; i++) {
                        $("#month").append("<option value='" + data[i] + "'>" + monthNumToName(data[i]) + "</option>");
                    }
                });
    }).change();
    
    $("#month").change(function () {
        if ($("#month").val() !== null) {
            var chart_id = window.location.pathname.split("/chart/")[1];
            if (chart_id === "finished_car") {
                $.get("/getfinishedstatistics", {year: $('#year').val(), month: $('#month').val()})
                    .done(function (data) {
                        drawChart(data);
                    });
            } else if (chart_id === "booked_car") {
                $.get("/getbookedstatistics", {year: $('#year').val(), month: $('#month').val()})
                    .done(function (data) {
                        drawChartPie(data);
                    });
            } else if (chart_id === "rent_car") {
                $.get("/getonrentstatistics", {year: $('#year').val(), month: $('#month').val()})
                    .done(function (data) {
                        drawChart3d(data);
                    });
            }
        }
    }).change();
    
    /*
    $("#statfinishedsubmit").click(function () {
        $.get("/getfinishedstatistics", {year: $('#year').val(), month: $('#month').val()})
                .done(function (data) {
                    drawChart(data);
                });
    });
    $("#statbookedsubmit").click(function () {
        $.get("/getbookedstatistics", {year: $('#year').val(), month: $('#month').val()})
                .done(function (data) {
                    drawChartPie(data);
                });
    });
    $("#statrentedsubmit").click(function () {
        $.get("/getonrentstatistics", {year: $('#year').val(), month: $('#month').val()})
                .done(function (data) {
                    drawChart3d(data);
                });
    });
    */

    function drawChart(data) {
        var categories = [];
        for (var i = 0; i < data.length; i++) {
            var object = {
                "name": data[i].name,
                "y": data[i].rentCountPerMonth,
                "drilldown": data[i].name
            };
            categories.push(object);
        }
        var myChart = Highcharts.chart('container', {
            chart: {
                type: 'column'
            },

            xAxis: {
                title: {
                    text: 'Car category'
                },
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Rent count'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true
                    }
                }
            },

            tooltip: {
                formatter: function () {
                    for (var i = 0; i < data.length; i++) {
                        if (this.key == data[i].name) {
                            return '<b>Description: </b><span>' + data[i].description + '</span><br>' +
                                    '<b>price/rent: </b><span>' + data[i].price + '</span>';
                        }
                    }
                }
            },

            "series": [
                {
                    "name": "Car Categories",
                    "colorByPoint": true,
                    "data": categories

                }
            ]

        });
    }

// Radialize the colors
Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    })
});

// Build the chart
function drawChartPie(data) {
    var categories = [];
    for (var i = 0; i < data.length; i++) {
        var object = {
            "name": data[i].name,
            "y": data[i].rentCountPerMonth

        };
        categories.push(object);
    }
    var mychartBooked = Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Booked Cars statistics'
        },
        tooltip: {
            formatter: function () {
                for (var i = 0; i < data.length; i++) {
                    if (this.key == data[i].name) {
                        return '<b>Description: </b><span>' + data[i].description + '</span><br>' +
                                '<b>price/rent: </b><span>' + data[i].price + '</span>';
                    }
                }
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    //format:'<b>{point.name}</b>:RentCount: {point.sum:.1f} db',
                    formatter: function () {
                        return '<b>' + this.point.name + '</b>:RentCount:' + this.y + 'db';
//                            for (var i = 0; i < data.length; i++) {
//                                if (this.y === data[i].rentCountPerMonth) {
//                                    return '<b>Description: </b><span>' + data[i].description + '</span><br>' +
//                                            '<b>price/rent: </b><span>' + data[i].price + '</span>';
//                                }
//                            }
                    },

                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                    connectorColor: 'silver'
                }
            }
        },
        series: [{
                name: 'Car Categories',
                data: categories
            }]
    });
    };
    function drawChart3d(data) {
        var categories = [];
        for (var i = 0; i < data.length; i++) {
            var object = {
                "name": data[i].name,
                "y": data[i].rentCountPerMonth

            };
            categories.push(object);
        }
        var mychartonrent = Highcharts.chart('container', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45
                }
            },
            title: {
                text: 'On rent cars'
            },
            tooltip: {
                formatter: function () {
                    for (var i = 0; i < data.length; i++) {
                        if (this.key == data[i].name) {
                            return '<b>Description: </b><span>' + data[i].description + '</span><br>' +
                                    '<b>price/rent: </b><span>' + data[i].price + '</span><br>' + 
                            '<b>Rent count: </b><span>' + data[i].rentCountPerMonth + '</span>';
                        }
                    }
                }
            },
            subtitle: {
                text: '3D donut in Highcharts'
            },
            plotOptions: {
                pie: {
                    innerSize: 100,
                    depth: 45
                }
            },
            series: [{
                    name: 'Car Categories',
                    data: categories
                }]
        });
    };
  
  });


