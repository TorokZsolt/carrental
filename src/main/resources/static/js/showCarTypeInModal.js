$(function () {
    $('#myModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var carType = button.data('type'); // Extract info from data-* attributes
        var carId = button.data('id'); // Extract info from data-* attributes
        var modal = $(this);
        modal.find('.modal-content #carType').text(carType);
        modal.find('.modal-content #carId').val(carId);
    });
});
